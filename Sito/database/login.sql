-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Gen 28, 2019 alle 02:17
-- Versione del server: 10.1.37-MariaDB
-- Versione PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `credenziali`
--

CREATE TABLE `credenziali` (
  `Username` char(32) COLLATE utf8_spanish_ci NOT NULL,
  `Pass` char(32) COLLATE utf8_spanish_ci NOT NULL,
  `tipoUtente` char(1) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Confermato` char(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dump dei dati per la tabella `credenziali`
--

INSERT INTO `credenziali` (`Username`, `Pass`, `tipoUtente`, `Confermato`) VALUES
('admin', 'admin', 'm', 'y'),
('Chiani-GB', 'bseqQYrw', 'f', 'y'),
('ciao', 'a', 'f', 'y'),
('Conad-UC', 'VzwhK1az', 'f', 'y'),
('erny-sury', 'MlWYSLux', 'a', 'n'),
('glock', 'F7rPa406', 'a', 'y'),
('gtrfsd', 'prxwtV5q', 'a', 'y'),
('Languorino-FC', 'Khbzu98V', 'f', 'y'),
('Lukas-EM', 'htCKjvvN', 'f', 'y'),
('marco', 'hsTIXgFR', 'a', 'n'),
('Mosaic-TT', 'fC10pV3D', 'f', 'y'),
('Piccio', 'a', 'a', 'y'),
('SigDory.97', 'mBnffPNj', 'a', 'y'),
('siggix', 'rCFuSP1F', 'a', 'y');

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti`
--

CREATE TABLE `prodotti` (
  `Username` char(32) COLLATE utf8_spanish_ci NOT NULL,
  `Categoria` char(100) COLLATE utf8_spanish_ci NOT NULL,
  `Nome` char(100) COLLATE utf8_spanish_ci NOT NULL,
  `Descrizione` char(250) COLLATE utf8_spanish_ci NOT NULL,
  `Prezzo` char(10) COLLATE utf8_spanish_ci NOT NULL,
  `PathImmagine` char(64) COLLATE utf8_spanish_ci NOT NULL,
  `Disp` char(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dump dei dati per la tabella `prodotti`
--

INSERT INTO `prodotti` (`Username`, `Categoria`, `Nome`, `Descrizione`, `Prezzo`, `PathImmagine`, `Disp`) VALUES
('Chiani-GB', 'Hamburger', 'Burger Romagnolo', 'Pane artigianale, 150gr. carne Romagnola IGP, crema allo squacquerone DOP ed erbette saltate in padella. Servito con patatine fritte', '9,00', 'Chiani-GB0-burger.jpg', 'Disponibile'),
('Chiani-GB', 'Hamburger', 'Cheeseburger', 'Pane artigianale, 300gr di carne Chianina, doppio cheddar, doppio bacon, pomodoro, insalata, salsa mayo e salsa BBQ artigianali. Servito con patatine fritte.', '12,00', 'Chiani-GB1-burger.jpg', 'Disponibile'),
('Chiani-GB', 'Hamburger', 'Burger Toscano', 'Pane artigianale, carne di puro suino toscano, lardo di Colonnata DOP, erbette saltate e salsa mayo artigianale. Servito con patatine fritte', '11,50', 'Chiani-GB2-burger.jpg', 'Disponibile'),
('Chiani-GB', 'Hot Dog', 'Hot Dog', 'Pane artigianale, wurstel di Chianina alla piastra', '8,00', 'Chiani-GB3-hotdog.jpg', 'Non disponibile'),
('ciao', 'Primo', 'Tagliatelle', 'Tagliatelle fatte a mano con abbondante sugo di carne', '5.50', 'ciao1-tagliatelle-ragu.jpg', 'Disponibile'),
('ciao', 'Secondi di carne', 'Costine alla bbq', 'Costine cotte al forno con ingenti spennelate di salsa bbq', '10.49', 'ciao2-costinebbq.jpg', 'Non disponibile'),
('ciao', 'Pizze', 'Diavola', 'Pizza con la base della margherita e in aggiunta fette di salame piccante', '6,00', 'ciao3-diavola.jpg', 'Non disponibile'),
('ciao', 'Pizze', 'Margherita', 'Buonissima pizza margherita', '4.50', 'ciao3-pizza.jpg', 'Disponibile'),
('ciao', 'Primo', 'Lasagne', 'Delicata lasagna con sfoglia tirata a mano, besciamella delicata e sugo alla bolognese', '6.00', 'ciao4-lasagne.jpg', 'Disponibile'),
('Conad-UC', 'Alimentari', 'Pizzetta', 'Pizzette confezionate di vari gusti', '2,00', 'Conad-UC0-pizzetta.jpg', 'Disponibile'),
('Conad-UC', 'Alimentari', 'Spianata', 'Fragrante spianata fatta da noi tutte le mattine', '2,00', 'Conad-UC1-spianata.jpg', 'Disponibile'),
('Conad-UC', 'Alimentari', 'Salumi', 'Ampia scelta di affettati al banco frigo', '3.00', 'Conad-UC2-affettato.jpg', 'Disponibile'),
('Languorino-FC', 'Pizze classiche', 'Margherita', 'Pizza margherita: pomodoro, mozzarella', '4,00', 'Languorino-FC0-margherita.jpg', 'Non disponibile'),
('Languorino-FC', 'Pizze classiche', 'Capricciosa', 'Pizza capricciosa: pomodoro, mozzarella, carciofini, cotto, olive nere', '8,00', 'Languorino-FC1-capricc.jpg', 'Disponibile'),
('Languorino-FC', 'Pizze classiche', 'Funghi e salsiccia', 'Pizza funghi e salsiccia: pomodoro, mozzarella, salsiccia, funghi', '7,00', 'Languorino-FC2-funesal.jpg', 'Disponibile'),
('Languorino-FC', 'Pizze speciali', 'Americana', 'Pizza americana: pomodoro, mozzarella, patatine, wurstel', '6,00', 'Languorino-FC3-america.jpg', 'Non disponibile'),
('Languorino-FC', 'Pizze speciali', 'Cardinale', 'Pizza cardinale: pomodoro, mozzarella, 4 formaggi, pancetta', '8,50', 'Languorino-FC4-cardinale.jpg', 'Disponibile'),
('Mosaic-TT', 'Weiss Beer', 'Delirium noctorium', 'Birra weiss dal colore ambrato, schiuma persistente e sapore dolciastro nel retrogusto', '5,00', 'Mosaic-TT0-noctorium.jpg', 'Disponibile'),
('Mosaic-TT', 'Weiss Beer', 'weihenstephaner hefeweissbier dunkel', 'Al naso ed al palato gli aromi sono dolci ed intesi, con in evidenza note fruttate di banana matura ben equilibrate dalle note più tostate di caramello e malto', '4,50', 'Mosaic-TT1-weihenstephaner.jpg', 'Disponibile'),
('Mosaic-TT', 'Weiss Beer', 'Schalchner weisse', 'Considerata il prodotto di punta di una piccola birreria che si trova nel sud della Germania, la Schwendl Brauerei', '3,75', 'Mosaic-TT2-Schalchner.jpg', 'Non disponibile'),
('Mosaic-TT', 'Weiss Beer', 'Kapuziner Weissbier Kristall-weizen', 'Birra dal colore dorato limpido con una schiuma bianca che presenta una buona ritenzione. Profumi di frutta (banana), lievito e spezie (chiodi di garofano)', '3,00', 'Mosaic-TT3-Kapuziner.jpg', 'Disponibile');

-- --------------------------------------------------------

--
-- Struttura della tabella `register`
--

CREATE TABLE `register` (
  `Nome` char(32) COLLATE utf8_spanish_ci NOT NULL,
  `Cognome` char(32) COLLATE utf8_spanish_ci NOT NULL,
  `Username` char(32) COLLATE utf8_spanish_ci NOT NULL,
  `Email` char(64) COLLATE utf8_spanish_ci NOT NULL,
  `Negozio` char(32) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Indirizzo` char(64) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Descrizione` varchar(1024) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Apertura` char(16) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Chiusura` char(16) COLLATE utf8_spanish_ci DEFAULT NULL,
  `PathLogo` char(64) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dump dei dati per la tabella `register`
--

INSERT INTO `register` (`Nome`, `Cognome`, `Username`, `Email`, `Negozio`, `Indirizzo`, `Descrizione`, `Apertura`, `Chiusura`, `PathLogo`) VALUES
('Giovanni', 'Buffetti', 'Chiani-GB', 'chiani@chiani.it', 'Chiani', 'via chiani 1', 'Hamburgeria con una vasta gamma di panini succulenti e birre alla spina', '11:30', '00:00', 'chiani.png'),
('ciao', 'ciao', 'ciao', 'ciao@ciao.ciao', 'ciao', 'via ciao 1', 'Salutiamo sempre', '07:00', '22:00', 'ciao-logo.jpg'),
('Umberto', 'Condaienti', 'Conad-UC', 'conad@conad.it', 'Conad', 'via conad 1', 'Ampio supermercato con prezzi bassi e fissi, facciamo del rapporto qualitÃ  prezzo il nostro punto di forza', '08:00', '20:30', 'conad.png'),
('ernesto', 'suricato', 'erny-sury', 'erny-sury@io.erny', NULL, NULL, NULL, NULL, NULL, NULL),
('Glauco', 'Bertozzi', 'glock', 'glaucobertozzi@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL),
('Nicholas', 'Bertozzi', 'gtrfsd', 'gfds@gfds.gf', NULL, NULL, NULL, NULL, NULL, NULL),
('Federico', 'Cestinelli', 'Languorino-FC', 'lang@lang.it', 'Languorino', 'via languorino 1', 'Pizzeria al piatto con vasta gamma di farciture e impasti bizzarri', '15:00', '22:15', 'languorino.jpg'),
('Edoardo', 'Marindo', 'Lukas-EM', 'lukas@lukas.it', 'Lukas Pizza', 'via lukas 1', 'Pizzeria con servizio a domicilio', '09:00', '23:30', 'lukasPizza.jpg'),
('marco', 'marco', 'marco', 'marco@marco.marco', NULL, NULL, NULL, NULL, NULL, NULL),
('Tiziano', 'Tinellazzi', 'Mosaic-TT', 'mosaic@mosaic.it', 'Mosaic', 'vai mosaic 1', 'Ampia gamma di birre alla spina con possibile serata giochi', '08:00', '23:59', 'mosaic.jpg'),
('Nicholas', 'Bertozzi', 'Piccio', 'bertozpiccio@gmail.commm', NULL, NULL, NULL, NULL, NULL, NULL),
('Francesco', 'Amadori', 'SigDory.97', 'francesco.a97@outlook.it', NULL, NULL, NULL, NULL, NULL, NULL),
('sisgismondo', 'bubilo', 'siggix', 'bertozpiccio@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `spedizione`
--

CREATE TABLE `spedizione` (
  `costo` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dump dei dati per la tabella `spedizione`
--

INSERT INTO `spedizione` (`costo`) VALUES
(4.5);

-- --------------------------------------------------------

--
-- Struttura della tabella `storico`
--

CREATE TABLE `storico` (
  `ID_ordine` int(11) NOT NULL,
  `Fornitore` char(32) COLLATE utf8_spanish_ci NOT NULL,
  `Utente` char(32) COLLATE utf8_spanish_ci NOT NULL,
  `Prodotto` char(100) COLLATE utf8_spanish_ci NOT NULL,
  `Quantita` int(11) NOT NULL,
  `Stato` char(32) COLLATE utf8_spanish_ci NOT NULL,
  `Orario` char(5) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Luogo` char(32) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Dump dei dati per la tabella `storico`
--

INSERT INTO `storico` (`ID_ordine`, `Fornitore`, `Utente`, `Prodotto`, `Quantita`, `Stato`, `Orario`, `Luogo`) VALUES
(85, 'Conad-UC', 'Piccio', 'Pizzetta', 6, 'pagato', '11:45', 'Entrata C'),
(88, 'Chiani-GB', 'Piccio', 'Burger Romagnolo', 2, 'pagato', '14:30', 'Entrata B'),
(88, 'Chiani-GB', 'Piccio', 'Burger Toscano', 2, 'pagato', '14:30', 'Entrata B'),
(89, 'Languorino-FC', 'Piccio', 'Capricciosa', 5, 'pagato', '15:30', 'Entrata B'),
(90, 'Languorino-FC', 'Piccio', 'Capricciosa', 1, 'pagato', '20:30', 'Entrata B'),
(91, 'Mosaic-TT', 'Piccio', 'Delirium noctorium', 10, 'spedito', '23:30', 'Entrata C'),
(92, 'ciao', 'Piccio', 'Tagliatelle', 2, 'pagato', '12:0', 'Entrata C'),
(92, 'ciao', 'Piccio', 'Margherita', 2, 'pagato', '12:0', 'Entrata C');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `credenziali`
--
ALTER TABLE `credenziali`
  ADD PRIMARY KEY (`Username`);

--
-- Indici per le tabelle `prodotti`
--
ALTER TABLE `prodotti`
  ADD PRIMARY KEY (`PathImmagine`);

--
-- Indici per le tabelle `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`Username`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- Indici per le tabelle `spedizione`
--
ALTER TABLE `spedizione`
  ADD PRIMARY KEY (`costo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
