<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

?>



<!DOCTYPE html>
<html lang="it-IT" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Fornitori</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/general.css">
    <link rel="stylesheet" href="./css/header_nav.css">
    <link rel="stylesheet" href="./css/chart.css">
    <link rel="stylesheet" href="./css/view_fornitori.css">
    <link rel="stylesheet" href="./css/footer.css">
  </head>
  <body>
    <header>
      <a href="./fornitori.php">
        <figure>
          <img id="logo" src="./img/logo.png" alt="logo sito"/>
        </figure>
      </a>
      <figure class="right_figure">
        <img id="carrello" src="./img/carrello.jpg" alt="carrello acquisti"/>
      </figure>
      <nav id="navExtended">
          <div class="opac"><span><a href="./fornitori.php">Home</a></span></div>
          <div><span><a href="#">Modifica account</a></span></div>
          <div class="opac"><span><a href="#">Riepilogo ordine</a></span></div>
          <div><span><a href="./index.php">Logout <span class="fas fa-sign-out-alt"></span></a></span></div>
      </nav>
      <div class="container-toggle">
        <div class="toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </div>
      </div>
    </header>
    <nav id="nav-collapsed">
      <ul class="">
        <li class="opac"><a href="./fornitori.php">Home</a></li>
        <li><a href="#">Modifica account</a></li>
        <li  class="opac"><a href="#">Riepilogo ordine</a></li>
        <li><a href="./index.php">Logout <span class="fas fa-sign-out-alt"></span></a></li>
      </ul>
    </nav>
    <div class="chart">

    </div>
    <section id="container-section">
      <div class="title">
        <h1>Elenco fornitori</h1>
      </div>
      <section id="container-fornitori">
        <?php
          $sql = "SELECT * FROM register WHERE Apertura IS NOT NULL";
          $result = $conn->query($sql);
          while ($row = $result->fetch_assoc()) {
              $user = $row['Username'];
        ?>
          <section class="fornitore col-xs-11 col-sm-5 ">
            <div class="container-img-forn">
              <a href="pr.php" onclick="setCookie('fornitore', '<?php echo $user?>', 1)" ><figure>
                <?php $percorso = "./img/" . $row['PathLogo']; ?>
                <img class="img-fonr" src="<?php echo $percorso; ?>" alt="immagine principale fornitore">
                <!--<img class="img-fonr" src="./img/fornitore.jpg" alt="immagine principale fornitore"/> -->
              </figure></a>

            </div>
            <section class="details">
              <a href="pr.php" onclick="setCookie('fornitore', '<?php echo $user?>', 1)" > <h4><?php echo $row["Negozio"];?></h4></a>
              <p class="type"><?php echo $row["Descrizione"];?></p>
            </section>
          </section>
            <?php
            }
            $conn->close(); ?>
      </section>
    </section>

    <footer>
      <section class="subtitles-footer">
        <span class="title-forn">Contattaci</span>
        <div id="information">
          <span>Telefono: +39 123456789</span><span>Email: <a href="mailto:campus.gourmet.unibo@gmail.com?Subject=Help%20from%20website">campus.gourmet.unibo@gmail.com</a></span>
        </div>

      </section>
      <section class="subtitles-footer">
        <span class="title-forn">Tipologie di cucina</span>
        <div id="typologies">
          <span>PIADINERIA PIZZERIA HABURGERIA BIRRERIA SUPERMERCATI DISCOUNT CUCINA MEDITERRRANEA</span>
        </div>
      </section>
      <section class="subtitles-footer">
          <span class="title-forn">Su di noi</span>
          <p>Il nostro impegno è offrire un servizio vario e soddisfacente per tutti gli utenti del campus</p>
      </section>

    </footer>


    <!-- JQUERY E BOOTSTRAP JS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="./js/scrollNav.js"></script>
    <script src="./js/chart.js"></script>
    <script src="./js/click_fornitori.js"></script>
  </body>
</html>
