<?php
$usernameFornitore = $_COOKIE["fornitore"];

$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$query = "SELECT * FROM register WHERE Username = \"$usernameFornitore\"";
$result = $conn->query($query);

while($row = $result-> fetch_assoc()) {
  $pathLogo = "../img/" . $row['PathLogo'];
  $negozio = $row['Negozio'];
  $indirizzo = $row['Indirizzo'];
  $desc = $row["Descrizione"];
  $apertura = $row['Apertura'];
  $chiusura = $row['Chiusura'];
}

//$conn->close();

?>

<!DOCTYPE html>
<html lang="it-IT" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Prodotti</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/header_nav.css">
    <link rel="stylesheet" href="../css/view_prodotti.css">
    <link rel="stylesheet" href="../css/index_forn.css">
  </head>
  <body>
    <header>
      <a href="./index_forn.php">
        <figure>
          <img id="logo" src="../img/logo.png" alt="logo sito"/>
        </figure>
      </a>
      <div id="navExtended">
          <div class="opac"><span><a href="./index_forn.php">Home</a></span></div>
          <div><span><a href="modify_data_forn.php">Modifica dati</a></span></div>
          <div class="opac"><span><a href="./modify_product.php">Modifica prodotti</a></span></div>
          <div><span><a href="./riepilogoOrdini.php">Riepilogo ordini</a></span></div>
          <div class="opac"><span><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></span></div>
      </div>
      <div class="container-toggle">
        <div class="toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </div>
      </div>
    </header>
    <nav id="nav-collapsed">
      <ul>
        <li class="opac"><a href="./index_forn.php">Home</a></li>
        <li><a href="modify_data_forn.php">Modifica dati</a></li>
        <li  class="opac"><a href="./modify_product.php">Modifica prodotti</a></li>
        <li><a href="./riepilogoOrdini.php">Riepilogo ordini</a></li>
        <li class="opac"><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></li>
      </ul>
    </nav>
    <section id="cont-info-forn">
      <div class="logo-forn">
        <figure>
          <img src="<?php echo $pathLogo?>" alt="logo fornitore corrente">
        </figure>
      </div>
      <div class="data-forn">
        <h1><?php echo $negozio ?></h1>
        <span><?php echo $indirizzo ?></span>
        <span><?php echo $desc ?></span>
      </div>
      <div class="data-forn hour">
        <span>Orari di apertura Lun - Ven</span>
        <span><?php echo $apertura ?> / <?php echo $chiusura ?></span>
      </div>
    </section>
    <section id="container-products">
      <a href="./modify_data_forn.php">
        <section class="category">
          <div class="category-title">
            <div class="container-title">
              <h2>Modifica dati</h2>
            </div>
          </div>
        </section>
      </a>
      <a href="./modify_product.php">
        <section class="category">
          <div class="category-title">
            <div class="container-title">
              <h2>Modifica prodotti</h2>
            </div>
          </div>
        </section>
      </a>
      <a href="./riepilogoOrdini.php">
        <section class="category">
          <div class="category-title">
            <div class="container-title">
              <h2>Riepilogo ordini</h2>
            </div>
          </div>
        </section>
      </a>
      <section class="category">
        <div class="category-title">
          <div class="container-title">
            <h2>Elenco prodotti attuali</h2>
          </div>
          <div class="container-icon down">
            <span class="fas fa-angle-down"></span>
          </div>
          <div class="container-icon up">
          <span class="fas fa-angle-up"></span>
          </div>
        </div>
      </section>




      <!--SUB-CATEGORIES-->
      <section class="subcategories">
        <?php
        $queryCategorie = "SELECT Categoria FROM prodotti WHERE Username = \"$usernameFornitore\"";
        $resultCategorie = $conn->query($queryCategorie);

        $tmpcat = "";
        while($rowCat = $resultCategorie-> fetch_assoc()) {
          /* controllo se la categoria è stata già vagliata ***********************************/
          /* viene effettuato questo controllo in quanto la categoria non è una chiave primaria
          quindi verrebbe creata una categoria per ogni prodotto presente in essa */

          $categoria = $rowCat["Categoria"];
          $ris = strpos($tmpcat, $categoria);
          if(strpos($tmpcat, $categoria) === FALSE) {
            $tmpcat .= $categoria;
          /* fine controllo categoria *********************************************************/
        ?>
        <section class="sub-category">
          <div class="category-title">
            <div class="container-title">
              <h3><?php echo $rowCat["Categoria"] ?></h3>
            </div>
            <div class="container-icon down">
              <span class="fas fa-angle-down"></span>
            </div>
            <div class="container-icon up">
            <span class="fas fa-angle-up"></span>
            </div>
          </div>
        </section>

        <!-- CONTAINER-DELLE-CATEGORIE-->
        <div class="container-products">
          <?php
          $categoria = $rowCat["Categoria"];
          $queryProdotti = "SELECT * FROM prodotti WHERE Categoria = \"$categoria\" AND Username = \"$usernameFornitore\"";
          $resultProdotti = $conn->query($queryProdotti);

          while($rowProd = $resultProdotti->fetch_assoc()) {
          ?>
          <section class="row-product">
            <div class="container-information">
              <div class="cont-img-product">
                <figure>
                  <?php $pathImage = "../img/prodotti/" . $rowProd["PathImmagine"]; ?>
                  <img class="img-product" src="<?php echo $pathImage ?>" alt="immagine prodotto">
                </figure>
              </div>
              <div class="information-product">
                <span class="title-forn"><?php echo $rowProd["Nome"] ?> - &nbsp;<span class="price"><?php echo $rowProd["Prezzo"] ?>€</span></span>
                <span><?php echo $rowProd["Descrizione"] ?></span>
              </div>
            </div>
          </section>
          <?php
          }
          ?>
        </div>
        <?php
          }
        }
        $conn->close();
        ?>
      </section>




    </section>


    <!-- JQUERY E BOOTSTRAP JS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="../js/scrollNav.js"></script>
    <script src="../js/index_forn.js"></script>
    <script src="../js/chart.js"></script>
  </body>
</html>
