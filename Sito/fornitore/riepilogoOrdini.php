<?php


$usernameFornitore = $_COOKIE["fornitore"];


$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

?>

<!DOCTYPE html>
<html lang="it-IT" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Riepilogo prodotti</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/header_nav.css">
    <link rel="stylesheet" href="../css/riepilogoOrdiniForn.css">
  </head>
  <body>
    <header>
      <a href="./index_forn.php">
        <figure>
          <img id="logo" src="../img/logo.png" alt="logo sito"/>
        </figure>
      </a>
      <div id="navExtended">
        <div class="opac"><span><a href="./index_forn.php">Home</a></span></div>
        <div><span><a href="modify_data_forn.php">Modifica dati</a></span></div>
        <div class="opac"><span><a href="./modify_product.php">Modifica prodotti</a></span></div>
        <div><span><a href="./riepilogoOrdini.php">Riepilogo ordini</a></span></div>
        <div class="opac"><span><a href="../index.php">Logout<span class="fas fa-sign-out-alt"></span></a></span></div>
      </div>
      <div class="container-toggle">
        <div class="toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </div>
      </div>
    </header>
    <nav id="nav-collapsed">
      <ul>
        <li class="opac"><a href="./index_forn.php">Home</a></li>
        <li><a href="./modify_data_forn.php">Modifica dati</a></li>
        <li  class="opac"><a href="./modify_product.php">Modifica prodotti</a></li>
        <li><a href="./riepilogoOrdini.php">Riepilogo ordini</a></li>
        <li class="opac"><a href="../index.php">Logout<span class="fas fa-sign-out-alt"></span></a></li>
      </ul>
    </nav>
    <section id="container-principal">
      <h1>Riepilogo ordini</h1>
      <section class="left">
        <section class="category">
          <div class="category-title">
            <div class="container-title">
              <h2>Ordini in attesa</h2>
            </div>
            <div class="container-icon down">
             <span class="fas fa-angle-down"></span>
            </div>
            <div class="container-icon up">
             <span class="fas fa-angle-up"></span>
            </div>
          </div>
        </section>
        <section class="listOrdini">
          <?php
            $query = "SELECT * FROM storico WHERE Fornitore='$usernameFornitore' AND Stato='pagato'";
            $result = $conn->query($query);
            $tmp = "";
            while($row = $result->fetch_assoc()) {
              $ordine = $row["ID_ordine"];
              $ordMod = $ordine . "e";
              if (strpos($tmp, $ordMod) === FALSE) {
              ?>
          <section class="sub-category">
            <div class="category-title">
              <div class="container-field-sub">
                <span class="title-forn"><?php echo $row["ID_ordine"]?></span>
              </div>
              <div class="container-field-sub">
                <span class="title-forn"><?php echo $row["Utente"]?></span>
              </div>
              <div class="container-field-sub">
                <span class="title-forn"><?php echo $row["Orario"]?></span>
              </div>
              <div class="changeStatusLarge">
                <button class="deliveryOrder" type="button" name="button" onclick="modifica_stato('<?php echo $ordine ?>')">Spedisci</button>
              </div>
              <div class="container-icon down-sub">
               <span class="fas fa-angle-down"></span>
              </div>
              <div class="container-icon up-sub">
             <span class="fas fa-angle-up"></span>
              </div>
            </div>
          </section>
          <section class="listProdotti">
            <?php
              $query2 = "SELECT * FROM storico WHERE ID_ordine='$ordine' ";
              $result2 = $conn->query($query2);
              while($row2 = $result2->fetch_assoc()) {
                $ordine = $row["ID_ordine"];
            ?>
            <div class="rowProdotto">
              <div class="container-field-prodotto">
                <span class="title-forn"><?php echo $row2["Prodotto"]?></span>
              </div>
              <div class="container-field-qta">
                <span class="title-forn"><?php echo $row2["Quantita"]?></span>
              </div>
            </div>
            <?php
            }
            ?>
            <div class="changeStatus">
              <button class="deliveryOrder" type="button" name="button" onclick="modifica_stato('<?php echo $ordine ?>')">Spedisci</button>
            </div>
          </section>
              <?php
                  }
                  $tmp .= $ordMod;
                }
              ?>
        </section>
      </section>
      <section class="right">
        <section class="category">
          <div class="category-title">
            <div class="container-title">
              <h2>Ordini spediti</h2>
            </div>
            <div class="container-icon down">
             <span class="fas fa-angle-down"></span>
            </div>
            <div class="container-icon up">
           <span class="fas fa-angle-up"></span>
            </div>
          </div>
        </section>
        <section class="listOrdini">
          <?php
          $query = "SELECT * FROM storico WHERE Fornitore='$usernameFornitore' AND Stato='spedito'";
          $result = $conn->query($query);
          $tmp = "";
          while($row = $result->fetch_assoc()) {
            $ordine = $row["ID_ordine"];
            $ordMod = $ordine . "e";
            if (strpos($tmp, $ordMod) === FALSE) {
          ?>
          <section class="sub-category">
            <div class="category-title">
              <div class="container-field-sub">
                <span class="title-forn"><?php echo $row["ID_ordine"]?></span>
              </div>
              <div class="container-field-sub">
                <span class="title-forn"><?php echo $row["Utente"]?></span>
              </div>
              <div class="container-field-sub">
                <span class="title-forn"><?php echo $row["Orario"]?></span>
              </div>
              <div class="container-icon down-sub">
               <span class="fas fa-angle-down"></span>
              </div>
              <div class="container-icon up-sub">
               <span class="fas fa-angle-up"></span>
              </div>
            </div>
          </section>
          <section class="listProdotti">
            <?php
              $query2 = "SELECT * FROM storico WHERE ID_ordine='$ordine' ";
              $result2 = $conn->query($query2);
              while($row2 = $result2->fetch_assoc()) {
                $ordine = $row["ID_ordine"];
            ?>
            <div class="rowProdotto">
              <div class="container-field-prodotto">
                <span class="title-forn"><?php echo $row2["Prodotto"]?></span>
              </div>
              <div class="container-field-qta">
                <span class="title-forn"><?php echo $row2["Quantita"]?></span>
              </div>
            </div>

            <?php
            }
            ?>
          </section>
          <?php
            }
            $tmp .= $ordMod;
          }
          ?>
        </section>
      </section>
    </section>

    <!-- JQUERY E BOOTSTRAP JS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/scrollNav.js"></script>
    <script type="text/javascript" src="../js/riepilogoOrdini.js"></script>
  </body>
</html>
