<?php
$usernameFornitore = $_COOKIE["fornitore"];

$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$query = "SELECT * FROM register WHERE Username = \"$usernameFornitore\"";
$result = $conn->query($query);
$row = $result->fetch_assoc();
$nome = $row["Nome"];
$cognome = $row["Cognome"];
$negozio = $row["Negozio"];
$apertura = $row["Apertura"];
$chiusura = $row["Chiusura"];
$ap = explode(":", $apertura);
$imm = "../img/" . $row['PathLogo'];
$ch = explode(":", $chiusura);
$conn->close();
?>


<!DOCTYPE html>
<html lang="it-IT" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Modifica dati fornitore</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/header_nav.css">
    <link rel="stylesheet" href="../css/mod_data_forn.css">
    <link rel="stylesheet" href="../css/confirmDialog.css">
  </head>
  <body>
    <header>
      <a href="./index_forn.php">
        <figure>
          <img id="logo" src="../img/logo.png" alt="logo sito"/>
        </figure>
      </a>
      <div id="navExtended">
          <div class="opac"><span><a href="./index_forn.php">Home</a></span></div>
          <div><span><a href="#">Modifica dati</a></span></div>
          <div class="opac"><span><a href="./modify_product.php">Modifica prodotti</a></span></div>
          <div><span><a href="./riepilogoOrdini.php">Riepilogo ordini</a></span></div>
          <div class="opac"><span><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></span></div>
      </div>
      <div class="container-toggle">
        <div class="toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </div>
      </div>
    </header>
    <nav id="nav-collapsed">
      <ul>
        <li class="opac"><a href="./index_forn.php">Home</a></li>
        <li><a href="#">Modifica dati</a></li>
        <li  class="opac"><a href="./modify_product.php">Modifica prodotti</a></li>
        <li><a href="./riepilogoOrdini.php">Riepilogo ordini</a></li>
        <li class="opac"><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></li>
      </ul>
    </nav>
    <section id="container-principal" class="all_content">
      <h1>Modifica dati attivit&agrave;</h1>
      <section id="container_mod">
        <form class="form_modForn" action="../php/modifica_forn.php" method="post" enctype="multipart/form-data">
          <div class="row_mod row_img">
            <div class="cont_img">
              <figure>
                <img src="<?php echo $imm ?>" alt="immagine princiale fornitore">
              </figure>
            </div>
            <span class="fas fa-edit i_img"></span>
          </div>
          <div class="none">
            <div class="cont_input_file">
              <label for="newFile">Immagine</label>
              <input id="newFile" type="file" name="imgLogo" onchange="readURL(this)"/>
              <div class="cont_new_img">
                <figure>
                  <figcaption>Verranno rispettate queste proporzioni per la visualizzazione utente</figcaption>
                  <img id="img_new_forn" src="../img/grey.jpg" alt="nuova immagine princiale fornitore">
                </figure>
              </div>
              <button class="confirm_btn" type="button" name="button" onclick="fade()">Conferma</button>
            </div>
          </div>
          <div class="row_mod">
            <div class="cont_name">
              <label class="labelMod" for="nameForn">Nome</label>
              <input class="inputMod" id="nameForn" type="text" name="name" value="<?php echo $nome ?>" readonly/>
            </div>
            <span class="fas fa-edit row_i"></span>
          </div>
          <div class="row_mod">
            <div class="cont_surname">
              <label class="labelMod" for="surnameForn">Cognome</label>
              <input class="inputMod" id="surnameForn" type="text" name="surname" value="<?php echo $cognome ?>" readonly/>
            </div>
            <span class="fas fa-edit row_i"></span>
          </div>
          <div class="row_mod">
            <div class="cont_attivita">
              <label class="labelMod" for="attivitaForn">Nome attivit&agrave;</label>
              <input class="inputMod" id="attivitaForn" type="text" name="att" value="<?php echo $negozio ?>" readonly/>
            </div>
            <span class="fas fa-edit row_i"></span>
          </div>
          <!--<div class="row_mod">
            <div class="cont_giorni">
              <label class="labelMod" for="giorniForn">Giorni apertura</label>
              <div class="contInputCheck">
                <input class="check" id="attivitaForn" type="checkbox" name="day" value="Lun" disabled checked/>Lun
                <input class="check" id="attivitaForn" type="checkbox" name="day" value="Mar" disabled checked/>Mar
                <input class="check" id="attivitaForn" type="checkbox" name="day" value="Mer" disabled checked/>Mer
                <input class="check" id="attivitaForn" type="checkbox" name="day" value="Gio" disabled checked/>Gio
                <input class="check" id="attivitaForn" type="checkbox" name="day" value="Ven" disabled checked/>Ven
              </div>
            </div>
            <span class="fas fa-edit row_i"></span>
          </div>-->
          <div class="row_mod">
            <div class="cont_hour">
              <label class="labelMod">Orario apertura</label>
              <div class="contHourOpen">
                <label for="hourOpenFornH">h</label>
                <input class="hh" id="hourOpenFornH" type="number" name="hhOp" value="<?php echo $ap[0] ?>" readonly/>&nbsp;:&nbsp;
                <input class="hh" id="hourOpenFornM" type="number" name="mmOp" value="<?php echo $ap[1] ?>" readonly/>
                <label for="hourOpenFornM">m</label>
              </div>
            </div>
            <div class="cont_hour">
              <label class="labelMod">Orario chiusura</label>
              <div class="contHourClose">
                <label for="hourCloseFornH">h</label>
                <input class="hh" id="hourCloseFornH" type="number" name="hhCl" value="<?php echo $ch[0] ?>" readonly/>&nbsp;:&nbsp;
                <input class="hh" id="hourCloseFornM" type="number" name="mmCl" value="<?php echo $ch[1] ?>" readonly/>
                <label for="hourCloseFornM">m</label>
              </div>
            </div>
            <span class="fas fa-edit row_i"></span>
          </div>
          <div class="row_mod">
            <div class="cont_btn">
              <button class="btn_forn" type="button" name="delete" onclick="fadeInConfirm()">Elimina Account</button>
              <input class="btn_forn right" type="submit" name="submit" value="Modifica"/>
            </div>
          </div>
      </form>
      <div id="confirmDialog">
        <div class="cont_dialog">
          <span>Sei sicuro di voler eliminare l'account?</span>
          <div class="cont_btn_confirm">
            <button class="button left" type="button" name="buttonDeny" onclick="fadeOutConfirm()">Annulla</button>
            <button class="button right" type="button" name="buttonConfirm" onclick="open_script()">Conferma</button>
          </div>
        </div>
      </div>
      </section>
    </section>

  <!-- JQUERY E BOOTSTRAP JS-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="../js/scrollNav.js"></script>
  <script src="../js/modify_data_forn.js"></script>
  <script src="../js/confirmDialog.js"></script>
  </body>
</html>
