<?php

$nomeLast = $_COOKIE["prodotto"];
$usernameFornitore = $_COOKIE["fornitore"];
$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$query = "SELECT * FROM prodotti WHERE Username = \"$usernameFornitore\" AND Nome = '$nomeLast'";
$result = $conn->query($query);
while($row = $result->fetch_assoc()) {
  $categoria = $row["Categoria"];
  $price = $row["Prezzo"];
  $disp = $row["Disp"];
  $imm = $row["PathImmagine"];
}
$conn->close();
 ?>

<!DOCTYPE html>
<html lang="it-IT" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Modifica singolo prodotto</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/header_nav.css">
    <link rel="stylesheet" href="../css/mod_data_forn.css">
    <link rel="stylesheet" href="../css/modify_single_product.css">
  </head>
  <body>
    <header>
      <a href="./index_forn.php">
        <figure>
          <img id="logo" src="../img/logo.png" alt="logo sito"/>
        </figure>
      </a>
      <div id="navExtended">
          <div class="opac"><span><a href="./index_forn.php">Home</a></span></div>
          <div><span><a href="./modify_data_forn.php">Modifica dati</a></span></div>
          <div class="opac"><span><a href="./modify_product.php">Modifica prodotti</a></span></div>
          <div><span><a href="./riepilogoOrdini.php">Riepilogo ordini</a></span></div>
          <div class="opac"><span><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></span></div>
      </div>
      <div class="container-toggle">
        <div class="toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </div>
      </div>
    </header>
    <nav id="nav-collapsed">
      <ul>
        <li class="opac"><a href="./index_forn.php">Home</a></li>
        <li><a href="./modify_data_forn.php">Modifica dati</a></li>
        <li  class="opac"><a href="./modify_product.php">Modifica prodotti</a></li>
        <li><a href="./riepilogoOrdini.php">Riepilogo ordini</a></li>
        <li class="opac"><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></li>
      </ul>
    </nav>
    <section id="container-principal" class="all_content">
      <h1>Modifica dati prodotto</h1>
      <section id="container_mod">
        <form class="form_modForn" action="../php/modify_product_selected.php" method="post" enctype="multipart/form-data">
          <div class="row_mod row_img">
            <div class="cont_img">
              <figure>
                <img src="../img/prodotti/<?php echo $imm; ?>" alt="immagine princiale prodotto">
              </figure>
            </div>
            <span class="fas fa-edit i_img"></span>
          </div>
          <div class="none">
            <div class="cont_input_file">
              <label for="id_input_img">Immagine</label>
              <input id="id_input_img" type="file" name="imgLogo" onchange="readURL(this)"/>
              <div class="cont_new_img">
                <figure>
                  <figcaption>Verranno rispettate queste proporzioni per la visualizzazione utente</figcaption>
                  <img id="img_new_forn" src="../img/grey.jpg" alt="nuova immagine princiale fornitore">
                </figure>
              </div>
              <button class="confirm_btn" type="button" name="button" onclick="fade()">Conferma</button>
            </div>
          </div>
          <div class="row_mod">
            <div class="cont_name">
              <label class="labelMod" for="nameProduct">Nome</label>
              <input class="inputMod" id="nameProduct" type="text" name="nameProduct" value="<?php echo $nomeLast ?>" readonly/>
            </div>
            <span class="fas fa-edit row_i"></span>
          </div>
          <div class="row_mod">
            <div class="cont_cat">
              <label class="labelMod" for="catProd">Categoria</label>

              <select class="inputMod" id="catProd" name="catProd" disabled>
                <option value="<?php echo  $categoria?>" selected="selected"><?php echo $categoria; ?></option>
                  <?php
                    $usernameFornitore = $_COOKIE["fornitore"];
                    $servername = "localhost";
                    $username = "root";
                    $password = "";
                    $database = "login";

                    // Create connection
                    $conn = mysqli_connect($servername, $username, $password, $database);

                    $query = "SELECT Categoria FROM prodotti WHERE Username = \"$usernameFornitore\"";
                    $result = $conn->query($query);
                    $tmpcat = "";
                    while($row=$result->fetch_assoc()) {
                      $cat = $row['Categoria'];
                      $ris = strpos($tmpcat, $cat);
                      if(strpos($tmpcat, $cat) === FALSE) {
                        $tmpcat .= $cat;
                      /* fine controllo categoria *********************************************************/
                    ?>
                <option value="<?php echo  $cat?>"><?php echo $cat ?></option>
                    <?php
                      }
                    }
                    $conn->close();
                    ?>
                <option value="new">Nuova Categoria</option>
              </select>

              <!-- <select class="inputMod" id="catProd" name="catProd" disabled>
                <option value="categoriaAttuale" selected>Categoria prodotto</option>
                <option value="categoria">Categoria 2</option>
                <option value="new">Nuova categoria</option>
              </select> -->
              <input type="hidden" name="catProdHidden" value="categoriaAttuale"/>
            </div>
            <span class="fas fa-edit row_i"></span>
          </div>
          <div class="row_mod">
            <div class="cont_new_cat">
              <label class="labelMod" for="newCat">Nuova Categoria</label>
              <input class="inputMod" id="newCat" type="text" name="newCatProd" value="" disabled/>
            </div>
          </div>
          <div class="row_mod">
            <div class="cont_price">
              <label class="labelMod" for="priceProd">Prezzo</label>
              <input class="inputMod" id="priceProd" type="text" name="att" value="<?php echo $price ?>" readonly/>
            </div>
            <span class="fas fa-edit row_i"></span>
          </div>
          <div class="row_mod">
            <div class="cont_disp">
              <label class="labelMod" for="dispProd">Disponibilit&agrave;</label>
              <select class="inputMod" id="dispProd" name="dispProd" disabled>
                <option value="Disponibile">Disponibile</option>
                <option value="Non disponibile">Non disponibile</option>
              </select>
              <input type="hidden" name="dispProdHidden" value="dispAttuale"/>
            </div>
            <span class="fas fa-edit row_i"></span>
          </div>
          <div class="row_mod">
            <div class="cont_btn">
              <input class="btn_forn" type="submit" name="submit" value="Modifica"/>
            </div>
          </div>
      </form>
      </section>
    </section>

  <!-- JQUERY E BOOTSTRAP JS-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="../js/scrollNav.js"></script>
  <script src="../js/modify_single_product.js"></script>
  </body>
</html>
