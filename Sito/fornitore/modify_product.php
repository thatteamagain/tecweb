<?php
$usernameFornitore = $_COOKIE["fornitore"];

$incrementalID = 0;

$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$query = "SELECT * FROM register WHERE Username = \"$usernameFornitore\"";
$result = $conn->query($query);

?>





<!DOCTYPE html>
<html lang="it-IT" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Modifica Prodotti</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/header_nav.css">
    <link rel="stylesheet" href="../css/view_prodotti.css">
    <link rel="stylesheet" href="../css/modify_product.css">
  </head>
  <body>
    <header>
      <a href="./index_forn.php">
        <figure>
          <img id="logo" src="../img/logo.png" alt="logo sito"/>
        </figure>
      </a>
      <div id="navExtended">
          <div class="opac"><span><a href="./index_forn.php">Home</a></span></div>
          <div><span><a href="modify_data_forn.php">Modifica dati</a></span></div>
          <div class="opac"><span><a href="./modify_product.php">Modifica prodotti</a></span></div>
          <div><span><a href="./riepilogoOrdini.php">Riepilogo ordini</a></span></div>
          <div class="opac"><span><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></span></div>
      </div>
      <div class="container-toggle">
        <div class="toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </div>
      </div>
    </header>
    <nav id="nav-collapsed">
      <ul>
        <li class="opac"><a href="./index_forn.php">Home</a></li>
        <li><a href="modify_data_forn.php">Modifica dati</a></li>
        <li  class="opac"><a href="./modify_product.php">Modifica prodotti</a></li>
        <li><a href="./riepilogoOrdini.php">Riepilogo ordini</a></li>
        <li class="opac"><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></li>
      </ul>
    </nav>
    <?php
      while($row = $result->fetch_assoc()) {

    ?>
    <section id="cont-info-forn">
      <div class="logo-forn">
        <figure>
          <?php $pathLogo = "../img/" . $row["PathLogo"]; ?>
          <img src="<?php echo $pathLogo ?>" alt="logo fornitore corrente">
        </figure>
      </div>
      <div class="data-forn">
        <h1><?php echo $row["Negozio"] ?></h1>
        <span><?php echo $row["Indirizzo"] ?></span>
        <span><?php echo $row["Descrizione"] ?></span>
      </div>
      <div class="data-forn hour">
        <span>Orari di apertura Lun - Ven</span>
        <span><?php echo $row["Apertura"] ?> / <?php echo $row["Chiusura"] ?></span>
      </div>
    </section>
    <?php
      }
    ?>
    <section id="container-products">
      <section class="category">
        <a href="./new_product.php">
          <div class="category-title">
            <div class="container-title">
              <h2>Aggiungi nuovo prodotto</h2>
            </div>
          </div>
        </a>
      </section>

      <?php
      $queryCategorie = "SELECT Categoria FROM prodotti WHERE Username = \"$usernameFornitore\"";
      $resultCategorie = $conn->query($queryCategorie);

      $tmpcat = "";
      while($rowCat = $resultCategorie-> fetch_assoc()) {
        /* controllo se la categoria è stata già vagliata ***********************************/
        /* viene effettuato questo controllo in quanto la categoria non è una chiave primaria
        quindi verrebbe creata una categoria per ogni prodotto presente in essa */

        $categoria = $rowCat["Categoria"];
        $ris = strpos($tmpcat, $categoria);
        if(strpos($tmpcat, $categoria) === FALSE) {
          $tmpcat .= $categoria;
        /* fine controllo categoria *********************************************************/
      ?>
      <section class="category">
        <div class="category-title">
          <div class="container-title">
            <h2><?php echo $categoria ?></h2>
          </div>
          <div class="modify_category_large">
            <label for="newCatLg<?php echo $incrementalID;?>">Nuova</label>
            <input id="newCatLg<?php echo $incrementalID;?>" class="newCat" type="text" name="newCat" value="" placeholder="Inserisci nuovo nome categoria"/>
            <input  class="button setNewCat" type="submit" onclick="modificaCat('<?php echo $categoria ?>', this)" name="confirm" value="Conferma">
          </div>
          <button class="button modify_btn modify_btn_cat" type="button" name="modify_category">Modifica categoria</button>
          <div class="container-icon plus">
            <span class="fas fa-plus"></span>
          </div>
          <div class="container-icon minum">
            <span class="fas fa-minus"></span>
          </div>
        </div>
      </section>
      <div class="modify_category">
        <label for="newCatSm<?php echo $incrementalID;?>">Nuova</label>
        <input id="newCatSm<?php echo $incrementalID; $incrementalID++;?>" class="newCat" type="text" name="newCat" value="" placeholder="Inserisci nuovo nome categoria"/>
        <input class="button setNewCat" type="submit" onclick="modificaCat('<?php echo $categoria ?>', this)" name="confirm" value="Conferma">
      </div>



      <div class="container-products">
        <?php
        $categoria = $rowCat["Categoria"];
        $queryProdotti = "SELECT * FROM prodotti WHERE Categoria = \"$categoria\" AND Username = \"$usernameFornitore\"";
        $resultProdotti = $conn->query($queryProdotti);

        while($rowProd = $resultProdotti->fetch_assoc()) {
        ?>

        <section class="row-product">
          <div class="container-information">
            <div class="cont-img-product">
              <figure>
                <?php $pathImage = "../img/prodotti/" . $rowProd["PathImmagine"]; ?>
                <img class="img-product" src="<?php echo $pathImage ?>" alt="immagine prodotto">
              </figure>
            </div>
            <div class="information-product">
              <span class="title-forn"><?php echo $rowProd["Nome"] ?> - &nbsp;<span class="price"><?php echo $rowProd["Prezzo"] ?>€</span></span>
              <span><?php echo $rowProd["Descrizione"] ?></span>
            </div>
          </div>
          <div class="buy-product">
            <button class="button modify_btn" type="button" onclick="modifica('<?php echo $rowProd['Nome']; ?>')" name="modify">Modifica</button>
            <button class="button delete_btn" type="button" onclick="cancella('<?php echo $rowProd['Nome']; ?>')" name="delete">Cancella</button>
          </div>
        </section>
        <?php
        }
        ?>
      </div>
      <?php
        }
      }
      $conn->close();
      ?>
    </section>

    <!-- JQUERY E BOOTSTRAP JS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="../js/scrollNav.js"></script>
    <script src="../js/prodotti.js"></script>
    <script src="../js/modify_product.js"></script>
    <script>
      function modifica(value) {
        var scadenza = new Date();
        var adesso = new Date();
        scadenza.setTime(adesso.getTime() - 1);
        document.cookie = "prodotto = " + value + ";expires=" + scadenza.toGMTString();
        document.cookie = "prodotto = " + value + ";path=/;";
        location.replace("./modify_single_product.php");
      }
    </script>
    <script>
      function cancella(value) {
        var scadenza = new Date();
        var adesso = new Date();
        scadenza.setTime(adesso.getTime() - 1);
        document.cookie = "prodottoDelete = " + value + ";expires=" + scadenza.toGMTString();
        document.cookie = "prodottoDelete = " + value + ";path=/;";
        location.replace("../php/delete_product.php");
      }
    </script>
    <script>
      function modificaCat(lastCat, newCatTag) {
        newCat = $(newCatTag).prev().val();
        $.ajax({
          type: "POST",
          url:"../php/modify_cat.php",
          data: "lastCat=" + lastCat + "&newCat=" + newCat,
          success: function(result) {
          },
          error: function(){
            alert("Chiamata fallita!!!");
          }
        });
      }

    </script>
  </body>
</html>
