<!DOCTYPE html>
<html lang="it-IT" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Aggiungi Prodotto</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/header_nav.css">
    <link rel="stylesheet" href="../css/new_product.css">
  </head>
  <body>
    <header>
      <a href="./index_forn.php">
        <figure>
          <img id="logo" src="../img/logo.png" alt="logo sito"/>
        </figure>
      </a>
      <div id="navExtended">
          <div class="opac"><span><a href="./index_forn.php">Home</a></span></div>
          <div><span><a href="./modify_data_forn.php">Modifica dati</a></span></div>
          <div class="opac"><span><a href="./modify_product.php">Modifica prodotti</a></span></div>
          <div><span><a href="./riepilogoOrdini.php">Riepilogo ordini</a></span></div>
          <div class="opac"><span><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></span></div>
      </div>
      <div class="container-toggle">
        <div class="toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </div>
      </div>
    </header>
    <nav id="nav-collapsed">
      <ul>
        <li class="opac"><a href="./index_forn.php">Home</a></li>
        <li><a href="#">Modifica dati</a></li>
        <li  class="opac"><a href="./modify_product.php">Modifica prodotti</a></li>
        <li><a href="./riepilogoOrdini.php">Riepilogo ordini</a></li>
        <li class="opac"><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></li>
      </ul>
    </nav>
    <section id="container-section">
      <h1>Inserimento nuovo prodotto</h1>
      <form class="form_newP" action="../php/insertProduct.php" method="post" enctype="multipart/form-data">
        <div class="container-input">
          <label for="nameProduct">Nome</label>
          <input id="nameProduct" type="text" name="nameProduct" placeholder="Nome prodotto" required/>
        </div>
        <div class="container-input">
          <label for="descProduct">Descrizione</label>
          <input id="descProduct" type="text" name="descProduct" placeholder="Descrizione prodotto" required/>
        </div>
        <div class="container-input">
          <label for="catProduct">Categoria</label>
          <select id="catProduct" name="catProduct" required>
            <option selected label="Inserisci categoria"></option>
              <?php
                $usernameFornitore = $_COOKIE["fornitore"];
                $servername = "localhost";
                $username = "root";
                $password = "";
                $database = "login";

                // Create connection
                $conn = mysqli_connect($servername, $username, $password, $database);

                $query = "SELECT Categoria FROM prodotti WHERE Username = \"$usernameFornitore\"";
                $result = $conn->query($query);
                $tmpcat = "";
                while($row=$result->fetch_assoc()) {
                  $categoria = $row['Categoria'];
                  $ris = strpos($tmpcat, $categoria);
                  if(strpos($tmpcat, $categoria) === FALSE) {
                    $tmpcat .= $categoria;
                  /* fine controllo categoria *********************************************************/
                ?>
            <option value="<?php echo  $categoria?>"><?php echo $categoria ?></option>
                <?php
                  }
                }
                $conn->close();
                ?>
            <option value="new">Nuova Categoria</option>
          </select>
        </div>
        <div class="container-input">
          <label for="newCat">Nuova Categoria</label>
          <input id="newCat" type="text" name="newCat" disabled required/>
        </div>
        <div class="container-input">
          <label for="priceProduct">Prezzo</label>
          <input id="priceProduct" type="number" step="0.01" name="price" placeholder="0.00"/>
        </div>
        <div class="container-input">
          <label for="dispProduct">Disponibilit&agrave;</label>
          <select id="dispProduct" name="dispProduct" required size="1">
            <option label="Disponibilit&agrave;"></option>
            <option value="Disponibile">Disponibile</option>
            <option value="Non disponibile">Non disponibile</option>
          </select>
        </div>
        <div class="container-input">
          <label for="imgProduct">Immagine</label>
          <input id="imgProduct" type="file" name="img" onchange="readURL(this)" required/>
        </div>
        <figure class="figProduct">
          <figcaption>Verranno rispettate queste proporzioni per la visualizzazione utente</figcaption>
          <img class="imgProduct" id="imgP" src="../img/grey.jpg" alt="Immagine prodotto"/>
        </figure>
        <div class="submit_cont">
          <input class="button" type="submit" name="submit" value="Conferma"/>
        </div>
      </form>
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="../js/scrollNav.js"></script>
    <script src="../js/new_product.js"></script>
  </body>
  <!-- JQUERY E BOOTSTRAP JS-->
</html>
