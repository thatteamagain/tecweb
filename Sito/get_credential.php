<!DOCTYPE html>
<html lang="it-IT" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Login - Notifica mail</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./css/login.css">
    <link rel="stylesheet" href="./css/general.css">
    <link rel="stylesheet" href="./css/credential.css">
  </head>
  <body>
    <div class="container">
      <div class="logo-div">
        <figure>
          <img id="logo" src="./img/logo.png" alt="logo sito"/>
        </figure>
      </div>
      <section class="login-container">
        <section class="login">
          <section class="login-information">
            <h1>Inserisci la mail a cui inviare le credenziali di accesso del tuo account</h1>
            <form action="./php/send_email_account_recovery.php" method="post">
              <input id="mailToRecovery" type="email" name="mailToRecovery" placeholder="Mail" required="required"/>
              <button class="send_email" type="submit" name="send">Invia</button>
            </form>

          </section>
        </section>
      </section>
      <div class="logo-div second">
        <figure>
          <img id="logo" src="./img/logo-unibo.png" alt="logo sito"/>
        </figure>
      </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="./js/login.js"></script>
    <script type="text/javascript"> // RELOADS WEBPAGE WHEN MOBILE ORIENTATION CHANGES
    window.onorientationchange = function() {
            window.location.reload();
    };
    </script>
  </body>
</html>
