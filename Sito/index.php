<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


$var = false;
if (isset($_POST["username"]) && isset( $_POST["Password"])) {
  $servername = "localhost";
  $username = "root";
  $password = "";
  $database = "login";

  // Create connection
  $conn = mysqli_connect($servername, $username, $password, $database);

  $query = "DELETE FROM storico WHERE Stato='incompleto'";
  $result = $conn->query($query);

  // Check connection
  if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
  }
if (isset($_COOKIE["ID_ordine"])) {
      setcookie("ID_ordine", "", time()-99999, '/');
  }
if (isset($_COOKIE["ProductQuantity"])) {
      setcookie("ProductQuantity", "", time()-99999, '/');
  }
  if (isset($_COOKIE["prodottoDelete"])) {
        setcookie("prodottoDelete", "", time()-99999, '/');
    }

  $query = "SELECT * FROM credenziali";
  $result = $conn->query($query);

    while ($row = $result->fetch_assoc()) {
      if($row["Confermato"] === 'y') {
        if($row["Username"] === $_POST["username"]) {
          if($row["Pass"] === $_POST["Password"]) {
            if($row["tipoUtente"] === "a") {
              header("location: ./utente/elenco_fornitori.php");   //pagina a cuisarà rediretto l'utente
              setcookie("user", $_POST["username"], time()+3600);
            } else if($row["tipoUtente"] === "f") {
              header("location: ./fornitore/index_forn.php");   //pagina a cui sarà rediretto il fornitore
              setcookie("fornitore", $_POST["username"]);
            } else {
              header("location: ./admin_pane.php"); //pagina a cui sarà rediretto l'admin
            }
            exit;
          }
        }
      }
    }
    $var = true;
}
  ?>


<!DOCTYPE html>
<html lang="it-IT" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Login - Campus Gourmets</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./css/login.css">
    <link rel="stylesheet" href="./css/general.css">
  </head>
  <body>
    <div class="container">
      <div class="logo-div">
        <figure>
          <img id="logo-first" src="./img/logo.png" alt="logo sito"/>
        </figure>
      </div>
      <section class="login-container">
        <section class="login">
          <div class="option">
            <div class="login-window btn">
              <p>Login</p>
            </div>
            <div class="reg-window btn">
              <p>Registrati</p>
            </div>
          </div>
          <section class="login-information">
            <form action="index.php" method="post">
              <label for="username">Username</label>
              <input class="login-data" id="username" type="text" name="username" required="required"/>
              <label for="password">Password</label>
              <input class="login-data" id="password" type="password" name="Password" required="required"/>
              <button class="login-data" type="submit" name="login">Accedi</button>
              <a id="lost_password" href="./get_credential.php">Hai dimenticato la password?</a>
              <?php if ($var === true) { ?>
                <p id="invalid-credentials">Credenziali non valide</p>
              <?php }?>
            </form>
          </section>
          <section class="reg-information">

            <section>
              <fieldset class="information-block" id="chose-type">
                <div>
                  <label for="acquirente">Acquirente</label>
                  <input class="data-input" id="acquirente" type="radio" name="type" value="acq" checked="checked"/>
                </div>
                <div>
                  <label for="fornitore">Fornitore</label>
                  <input class="data-input" id="fornitore" type="radio" value="forn" name="type"/>
                </div>
              </fieldset>


              <form action="./php/registrationUser.php" method="post">
                <fieldset class="information-block user">
                  <section>
                    <label for="nameUser">Nome</label>
                    <input class="data-input" id="nameUser" type="text" name="nameUser" placeholder="Name" required="required"/>
                  </section>
                  <section>
                    <label for="surnameUser">Cognome</label>
                    <input id="surnameUser" type="text" name="surnameUser" placeholder="Surname" required="required"/>
                  </section>
                  <section>
                    <label for="usernameRegUser">Username</label>
                    <input id="usernameRegUser" onchange="myFunction1(
                      <?php
                        $servername = "localhost";
                        $username = "root";
                        $password = "";
                        $database = "login";
                        $var = '\'';
                        $apice = '\'';
                        // Create connection
                        $conn2 = mysqli_connect($servername, $username, $password, $database);
                        $query = 'SELECT * FROM credenziali';
                        $result = $conn2->query($query);
                        while($row = $result->fetch_assoc()) {
                          $var = $var . "?" . $row['Username'];
                        }
                        $var = $var . $apice;
                        echo $var;
                      ?>, this.value)" type="text" name="usernameRegUser" placeholder="Username" required="required"/>

                  </section>
                  <section>
                    <label for="mailRegUser">Mail</label>
                    <input id="mailRegUser" onchange="myFunction2(
                      <?php
                        $servername = "localhost";
                        $username = "root";
                        $password = "";
                        $database = "login";
                        $var = '\'';
                        $apice = '\'';
                        // Create connection
                        $conn2 = mysqli_connect($servername, $username, $password, $database);
                        $query = 'SELECT * FROM register';
                        $result = $conn2->query($query);
                        while($row = $result->fetch_assoc()) {
                          $var = $var . "?" . $row['Email'];
                        }
                        $var = $var . $apice;
                        echo $var;
                      ?>, this.value)" type="email" name="mailRegUser" placeholder="Mail" required="required"/>

                  </section>
                  <p id="cred_used_user"></p>
                  <button id="registration-submit-user" type="submit" name="reg-user">Registrati</button>
                </fieldset>
              </form>

              <form action="./php/registrationForn.php" method="post" enctype="multipart/form-data">
                <fieldset class="information-block forn">
                  <section>
                    <label for="nameForn">Nome</label>
                    <input class="data-input" id="nameForn" type="text" name="nameForn" placeholder="Name" required="required"/>
                  </section>
                  <section>
                    <label for="surnameForn">Cognome</label>
                    <input id="surnameForn" type="text" name="surnameForn" placeholder="Surname" required="required"/>
                  </section>
                  <section>
                    <label for="usernameRegForn">Username</label>
                    <input id="usernameRegForn" onchange="myFunction3(
                      <?php
                        $servername = "localhost";
                        $username = "root";
                        $password = "";
                        $database = "login";
                        $var = '\'';
                        $apice = '\'';
                        // Create connection
                        $conn2 = mysqli_connect($servername, $username, $password, $database);
                        $query = 'SELECT * FROM credenziali';
                        $result = $conn2->query($query);
                        while($row = $result->fetch_assoc()) {
                          $var = $var . "?" . $row['Username'];
                        }
                        $var = $var . $apice;
                        echo $var;
                      ?>, this.value)" type="text" name="usernameRegForn" placeholder="Username" required="required"/>
                  </section>
                  <section>
                    <label for="mailRegForn">Mail</label>
                    <input id="mailRegForn" onchange="myFunction4(
                      <?php
                        $servername = "localhost";
                        $username = "root";
                        $password = "";
                        $database = "login";
                        $var = '\'';
                        $apice = '\'';
                        // Create connection
                        $conn2 = mysqli_connect($servername, $username, $password, $database);
                        $query = 'SELECT * FROM register';
                        $result = $conn2->query($query);
                        while($row = $result->fetch_assoc()) {
                          $var = $var . "?" . $row['Email'];
                        }
                        $var = $var . $apice;
                        echo $var;
                      ?>, this.value)" type="email" name="mailRegForn" placeholder="Mail" required="required"/>
                  </section>
                  <section>
                    <label for="negozio">Negozio</label>
                    <input id="negozio" type="text" name="negozio" placeholder="Negozio" required="required"/>
                  </section>
                  <section>
                    <label for="indirizzo">Indirizzo</label>
                    <input id="indirizzo" type="text" name="indirizzo" placeholder="Indirizzo" required="required"/>
                  </section>
                  <section class="section_desc">
                    <label for="descrizione">Descrizione</label>
                    <textarea id="descrizione" name="descrizione" required="required" ></textarea>
                  </section>
                  <section class="hour-cont">
                    <span class="full-row">Orari Lun-Ven</span>
                    <div class="hour">
                      <label for="hourFornOpen">Apertura</label>
                      <input id="hourFornOpen" type="time" name="hourOpen" required="required"/>
                    </div>
                    <div class="hour">
                      <label for="hourFornClose">Chiusura</label>
                      <input id="hourFornClose" type="time" name="hourClose" required="required"/>
                    </div>
                  </section>
                  <section class="insertLogo">
                    <label class="full-row" for="logoForn">Inserisci Logo</label>
                    <input id="logoForn" type="file" name="file" required="required"/>
                  </section>
                  <p id="cred_used_forn"></p>
                  <button id="registration-submit-forn" type="submit" name="reg-forn">Registrati</button>
                </fieldset>
              </form>
            </section>
          </section>
        </section>
      </section>
      <div class="logo-div second">
        <figure>
          <img id="logo" src="./img/logo-unibo.png" alt="logo sito"/>
        </figure>
      </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="./js/login.js"></script>

  </body>
</html>
