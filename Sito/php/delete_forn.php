<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$usernameFornitore = $_COOKIE["fornitore"];
$queryDeleteCredenziali = "DELETE FROM credenziali WHERE Username = \"$usernameFornitore\"";
$queryDeleteProdotti = "DELETE FROM prodotti WHERE Username = \"$usernameFornitore\"";
$queryDeleteRegister = "DELETE FROM register WHERE Username = \"$usernameFornitore\"";
$queryImgProdotti = "SELECT PathImmagine FROM prodotti WHERE Username = \"$usernameFornitore\"";
$queryLogo = "SELECT PathLogo FROM register WHERE Username = \"$usernameFornitore\"";

unlink('../img/a.jpg'); //funziona e cancella l'immagine

/* elimino tutte le immagini dei prodotti dalla cartella prodotti */
$result = $conn->query($queryImgProdotti);
while($row = $result->fetch_assoc()) {
  $immagine = "../img/prodotti/" . $row["PathImmagine"];
  unlink($immagine);
}

/* elimino il logo del venditore dalla cartella img */
$result = $conn->query($queryLogo);
$row = $result->fetch_assoc();
$immagine = "../img/" . $row["PathLogo"];
unlink($immagine);

/* elimino fornitore dalla tabella delle credenziali */
$result = $conn->query($queryDeleteCredenziali);

/* elimino i prodotti dalla tabella dei prodotti */
$result = $conn->query($queryDeleteProdotti);

/* elimino fornitore dalla tabella della registrazione */
$result = $conn->query($queryDeleteRegister);

$conn->close();

/* elimino i cookie */
/* Actually, there is not a way to directly delete a cookie.
Just use setcookie with expiration date in the past, to trigger the removal mechanism in your browser */
unset($_COOKIE["firnitore"]);
// empty value and expiration one hour before
$res = setcookie("fornitore", '', time() - 3600);

header("location: ../index.php");
?>
