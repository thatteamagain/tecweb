<?php

/* connessione database per lo stockaggio delle informaizioni relative alle registrazioni, escluse le password */
$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
$nome = $_POST['nameForn'];
$cognome = $_POST['surnameForn'];
$user = $_POST['usernameRegForn'];
$email = $_POST['mailRegForn'];
$negozio = $_POST['negozio'];
$indirizzo = $_POST['indirizzo'];
$descrizione = $_POST['descrizione'];
$apertura = $_POST['hourOpen'];
$chiusura = $_POST['hourClose'];
$dir = '../img/';
$size = $_FILES['file']['size'];
if($size === 0) {
  die("immagine maggiore di 2Mb");
}

$userFileTmp = $_FILES['file']['tmp_name'];
$userFileName = $_FILES['file']['name'];
/*
$userFileType = $_FILES['file']['type'];
echo "$userFileTmp<br/>";
echo "$userFileName<br/>";
echo "$userFileType<br/>";
$destination = $dir . $userFileName;
*/
$cerca = ".";
$posizione = strpos($userFileName, $cerca);
$stringaEliminare = substr($userFileName, 0, $posizione);
$formatoImg = str_replace($stringaEliminare, "", $userFileName); //contiene anche il punto (esempio:  .img )
$userFileName = $user . "-logo" . $formatoImg;
$destination = $dir . $userFileName;
echo "$userFileName<br/>";


$query = "INSERT INTO register (Nome, Cognome, Username, Email, Negozio, Indirizzo, Descrizione, Apertura, Chiusura, PathLogo) VALUES ('$nome', '$cognome', '$user', '$email', '$negozio', '$indirizzo', '$descrizione', '$apertura', '$chiusura', '$userFileName')";
$result = $conn->query($query);
if ($result === FALSE) {
  echo "errore inserimento tabella di login";
}

move_uploaded_file($userFileTmp, $destination);
//$conn->close();







/* generazione password csuale da assegnare all'utente che si andrà a registrare */
$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
$pass = array(); //remember to declare $pass as an array
$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
for ($i = 0; $i < 8; $i++) {
    $n = rand(0, $alphaLength);
    $pass[] = $alphabet[$n];
}
$randomPass =  implode($pass); //turn the array into a string


/* connessione alla tabella per la gestione delle password e il relativo invio tramite email */
$query = "INSERT INTO credenziali (Username, Pass, tipoUtente, Confermato) VALUES ('$user', '$randomPass', 'f', 'n')";
$result = $conn->query($query);
if ($result === FALSE) {
  echo "errore inserimento tabella delle credennziali";
}

$conn->close();

/* invio per email delle credenziali per l'accesso al sito */
$email = trim($email); //eliminare spazi bianchi

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load composer's autoloader
require 'vendor/autoload.php';

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'campus.gourmet.unibo@gmail.com';                 // SMTP username
    $mail->Password = 'Campus2018';                           // SMTP password
    $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('campus.gourmet.unibo@gmail.com', 'Campus Gourmet');
    //$mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
    $mail->addAddress($email);               // Name is optional
    //$mail->addReplyTo('info@example.com', 'Information');
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    /*
    //Attachments
    $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
    */

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Campus Grormet: credenziali di accesso';
    $mail->Body = "<p>Gentile $nome $cognome,<br/>la ringraziamo per essersi iscritto al sito Campus Gourmet.<br/>";
    $mail->Body .= "Le sue credenziali di accesso sono:<br/>";
    $mail->Body .= "Username: $user<br/>";
    $mail->Body .= "Password: $randomPass<br/></br></p>";
    $mail->Body .= "<a href=\"http://localhost:8088/exercises/Sito/account_confirm.php\">Cliccare qui per confermare</a>";
    $mail->Body .= "<p><br/><br/><br/>Il team di Campus Gourmet le augura una buona permanenza all'interno del sito</p>";
    /*
    $mail->AltBody = "Gentile $nome,\nla ringraziamo per essersi iscritto al sito Campus Gourmet.\n"; //variants for non html client email
    $mail->AltBody .= "Le sue credenziali di accesso sono:\n";
    $mail->AltBody .= "Username: $username\n";
    $mail->AltBody .= "Password: $randomPass\n";
    $mail->AltBody .= "\n\n\nIl team di Campus Gourmet le augura una buona permanenza all'interno del sito";*/

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}

setcookie("registrazione", $user, time()+3600, '/');
header("location: ../notice_account.html");
?>
