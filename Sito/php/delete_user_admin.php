<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "login";
$user = $_COOKIE["user_deleted"];

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

if ($user === "admin") {
  header("location: ../admin_pane.php");
} else {
  $query = "SELECT tipoUtente FROM credenziali WHERE Username = '$user'";
  $result = $conn->query($query);
  if ($result === FALSE) {
    header("location: ../admin_pane.php");
  } else {
    $row=$result -> fetch_assoc();
    if ($row["tipoUtente"] === "a") {
      $queryDeleteCredenziali = "DELETE FROM credenziali WHERE Username = \"$user\"";
      $queryDeleteRegister = "DELETE FROM register WHERE Username = \"$user\"";

      /* elimino utente dalla tabella delle credenziali */
      $result = $conn->query($queryDeleteCredenziali);

      /* elimino fornitore dalla tabella della registrazione */
      $result = $conn->query($queryDeleteRegister);

      $conn->close();

      header("location: ../admin_pane.php");
    } else {
      $queryDeleteCredenziali = "DELETE FROM credenziali WHERE Username = \"$user\"";
      $queryDeleteProdotti = "DELETE FROM prodotti WHERE Username = \"$user\"";
      $queryDeleteRegister = "DELETE FROM register WHERE Username = \"$user\"";
      $queryImgProdotti = "SELECT PathImmagine FROM prodotti WHERE Username = \"$user\"";
      $queryLogo = "SELECT PathLogo FROM register WHERE Username = \"$user\"";

      /* elimino tutte le immagini dei prodotti dalla cartella prodotti */
      $result = $conn->query($queryImgProdotti);
      while($row = $result->fetch_assoc()) {
        $immagine = "../img/prodotti/" . $row["PathImmagine"];
        unlink($immagine);
      }

      /* elimino il logo del venditore dalla cartella img */
      $result = $conn->query($queryLogo);
      $row = $result->fetch_assoc();
      $immagine = "../img/" . $row["PathLogo"];
      unlink($immagine);

      /* elimino fornitore dalla tabella delle credenziali */
      $result = $conn->query($queryDeleteCredenziali);

      /* elimino i prodotti dalla tabella dei prodotti */
      $result = $conn->query($queryDeleteProdotti);

      /* elimino fornitore dalla tabella della registrazione */
      $result = $conn->query($queryDeleteRegister);

      $conn->close();
      header("location: ../admin_pane.php");
    }
  }
}

?>
