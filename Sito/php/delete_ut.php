<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$usernameutente = $_COOKIE["user"];
$queryDeleteCredenziali = "DELETE FROM credenziali WHERE Username = \"$usernameutente\"";
$queryDeleteRegister = "DELETE FROM register WHERE Username = \"$usernameutente\"";

/* elimino utente dalla tabella delle credenziali */
$result = $conn->query($queryDeleteCredenziali);

/* elimino fornitore dalla tabella della registrazione */
$result = $conn->query($queryDeleteRegister);

$conn->close();

/* elimino i cookie */
/* Actually, there is not a way to directly delete a cookie.
Just use setcookie with expiration date in the past, to trigger the removal mechanism in your browser */
unset($_COOKIE["user"]);
// empty value and expiration one hour before
$res = setcookie("user", '', time() - 3600);

header("location: ../index.php");
?>
