<?php

$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

$nomeProdottoPost = $_POST["nomeProdotto"];
$nomeFornitorePost = $_POST["nomeFornitore"];
$nomeUtentePost = $_POST["nomeUtente"];
$quantitaPost = $_POST["quantita"];



// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

/*
$queryCount = "SELECT COUNT(ID_ordine) AS cont FROM storico WHERE Utente='$nomeUtentePost' AND Stato='incompleto'";
$result = $conn->query($queryCount);
$row = $result->fetch_assoc();
if($row["cont"] === 0) {
  setcookie("ID_ordine", "", time()-3600, '/');
}*/
/* controllo se il prodotto che si vuole aggiungere al carrello sia disponibile */
$queryControlloDisponibilita = "SELECT Disp FROM prodotti WHERE Nome='$nomeProdottoPost' AND Username='$nomeFornitorePost'";
$resultDisp = $conn->query($queryControlloDisponibilita);
$rowDisp = $resultDisp->fetch_assoc();
if($rowDisp["Disp"] !== "Disponibile") {
  header('HTTP/1.1 500 INTERNAL Server Booboo');
  header('Content-Type: application/json; charset=UTF-8');
  //die(json_encode(array('message'=>'ERROR', 'code'=>1337)));
  die(json_encode("WARNING: is not permitted to add to the cart products not available"));
}

if(!isset($_COOKIE["ID_ordine"])) {
  $queryIDOrdine = "SELECT MAX(ID_ordine) AS count FROM storico";
  $result = $conn->query($queryIDOrdine);
  $row = $result->fetch_assoc();
  $id_o = $row["count"] + 1;
  setcookie("ID_ordine", $id_o, time()+3600, '/');
} else {
  $id_o = $_COOKIE["ID_ordine"];
  $queryCorrispondenzaFornitore = "SELECT Fornitore FROM storico WHERE ID_ordine = \"$id_o\"";
  $result = $conn->query($queryCorrispondenzaFornitore);
  $row = $result->fetch_assoc();
  $fornitoreDB = $row["Fornitore"];
    if($nomeFornitorePost !== $fornitoreDB) {
      header('HTTP/1.1 500 INTERNAL Server Booboo');
      header('Content-Type: application/json; charset=UTF-8');
      //die(json_encode(array('message'=>'ERROR', 'code'=>1337)));
      die(json_encode("ERROR: Is not permitted select product from other supplier"));
    }
}
/* controllo se il prodotto è già presente nella tabella dello storico, allora aggiorniamo la quantità */
$query = "SELECT COUNT(Quantita) AS quantita FROM storico WHERE ID_ordine=\"$id_o\" AND Prodotto=\"$nomeProdottoPost\"";
$result = $conn->query($query);
$row = $result->fetch_assoc();
/* controllo se in quell'ordine non sia già presente quel prodotto */
if($row["quantita"] !== "0") {
  $query = "UPDATE storico SET Quantita='$quantitaPost' WHERE ID_ordine=\"$id_o\" AND Prodotto=\"$nomeProdottoPost\"";
  $conn->query($query);
} else { /* se il prodotto è già presente, ne aggiorno solo la quantità */
  $query = "INSERT INTO storico (ID_ordine, Fornitore, Utente, Prodotto, Quantita, Stato, Orario, Luogo) VALUES ('$id_o', '$nomeFornitorePost', '$nomeUtentePost', '$nomeProdottoPost', '$quantitaPost', 'incompleto', NULL, NULL)";
  $conn->query($query);
}
/* elimino dallo storico degli ordini il prodotto la cui quantità è stata settata a 0 */
$queryDeleteZeroQuantity = "DELETE FROM storico WHERE ID_ordine='$id_o' AND Quantita='0'";
$conn->query($queryDeleteZeroQuantity);

$query = "SELECT Prodotto, Quantita, Fornitore FROM storico WHERE ID_ordine=\"$id_o\"";
$result = $conn->query($query);
$response = "ProductQuantity=§";
while($row = $result->fetch_assoc()) {
  $fornitore_per_imm = $row["Fornitore"];
  $prodotto_per_imm = $row["Prodotto"];
  $queryImm = "SELECT PathImmagine FROM prodotti WHERE Nome='$prodotto_per_imm' AND Username='$fornitore_per_imm'";
  $resultImm = $conn->query($queryImm);
  $rowImm = $resultImm->fetch_assoc();
  $temp = $row["Prodotto"] . "~" . $row["Quantita"] . "~" . $rowImm["PathImmagine"] . "§";
  $response .= $temp;
}
$response .= "; path=/";

$queryCount = "SELECT COUNT(ID_ordine) AS cont FROM storico WHERE Utente='$nomeUtentePost' AND Stato='incompleto'";
$result = $conn->query($queryCount);
$row = $result->fetch_assoc();
if($row["cont"] < 1) {
    setcookie("ID_ordine", $id_o, time()-3600, '/');
}

header('Content-Type: application/json; charset=UTF-8');
echo (json_encode($response));
$conn->close();
//header("location: ../utente/elenco_prodotti.php");





?>
