<?php

$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$mailToRecovery = $_POST["mailToRecovery"];
$queryCheckEmail = "SELECT COUNT(Email) AS num FROM register WHERE Email='$mailToRecovery'";
$result = $conn->query($queryCheckEmail);
$row = $result->fetch_assoc();
if($row["num"] > 0) {
  $queryTakeCredential = "SELECT Email, Username, Nome, Cognome FROM register WHERE Email='$mailToRecovery'";
  $result = $conn->query($queryTakeCredential);
  $row = $result->fetch_assoc();
  $email = $row["Email"];
  $username = $row["Username"];
  $name = $row["Nome"];
  $surname = $row["Cognome"];

  /* generazione password csuale da assegnare all'utente che si andrà a registrare */
  $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
  $pass = array(); //remember to declare $pass as an array
  $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
  for ($i = 0; $i < 8; $i++) {
      $n = rand(0, $alphaLength);
      $pass[] = $alphabet[$n];
  }
  $randomPass =  implode($pass); //turn the array into a string

  $queryUpdateCredential = "UPDATE credenziali SET Pass='$randomPass' WHERE Username='$username'";
  $result = $conn->query($queryUpdateCredential);
}
  $conn->close();

  /* invio per email recupero credenziali */
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;

  //Load composer's autoloader
  require 'vendor/autoload.php';

  $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
  try {
      //Server settings
      $mail->SMTPDebug = 0;                                 // Enable verbose debug output
      $mail->isSMTP();                                      // Set mailer to use SMTP
      $mail->Host = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
      $mail->SMTPAuth = true;                               // Enable SMTP authentication
      $mail->Username = 'campus.gourmet.unibo@gmail.com';   // SMTP username
      $mail->Password = 'Campus2018';                       // SMTP password
      $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
      $mail->Port = 587;                                    // TCP port to connect to

      //Recipients
      $mail->setFrom('campus.gourmet.unibo@gmail.com', 'Campus Gourmet');
      $mail->addAddress($email);                            // Name is optional

      //Content
      $mail->isHTML(true);                                  // Set email format to HTML
      $mail->Subject = 'Campus Grormet: recupero credenziali di accesso';
      $mail->Body = "<p>Gentile $name $surname,<br/>la informiano che è stato effettuato un recupero per il suo account del sito Campus Gourmet.<br/>";
      $mail->Body .= "Le sue nuove credenziali di accesso sono:<br/>";
      $mail->Body .= "Username: $username<br/>";
      $mail->Body .= "Password: $randomPass</p>";
      $mail->Body .= "<p><br/><br/>Il team di Campus Gourmet le augura una buona permanenza all'interno del sito</p>";

      $mail->send();
      echo 'Message has been sent';
  } catch (Exception $e) {
      echo 'Message could not be sent.';
      echo 'Mailer Error: ' . $mail->ErrorInfo;
  }

header("location: ../credential_send.html");

?>
