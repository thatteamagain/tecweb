<?php
$usernameFornitore = $_COOKIE["fornitore"];
$nomeLast = $_COOKIE["prodotto"];

$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$nome = $_POST["nameProduct"];
$price = $_POST["att"];
$disp = $_POST["dispProdHidden"];
if($disp == "dispAttuale") {
  $query = "SELECT Disp from prodotti WHERE Username='$usernameFornitore' AND Nome='$nomeLast'";
  $result = $conn->query($query);
  $row = $result->fetch_assoc();
  $disp = $row["Disp"];
}
if($_POST["catProd"] == "new") {
  $categoria = $_POST["newCatProd"];
} else {
  $categoria = $_POST["catProdHidden"];
  if($categoria == "categoriaAttuale") {
    $query = "SELECT Categoria from prodotti WHERE Username='$usernameFornitore' AND Nome='$nomeLast'";
    $result = $conn->query($query);
    $row = $result->fetch_assoc();
    $categoria = $row["Categoria"];
  }
}

$fileName = $_FILES['imgLogo']['name'];
$fileTmp = $_FILES['imgLogo']['tmp_name'];
$dir = '../img/prodotti/';
$imm = $fileName;
if ($imm === "") {
  $query = "SELECT PathImmagine from prodotti WHERE Username='$usernameFornitore' AND Nome='$nomeLast'";
  $result = $conn->query($query);
  $row = $result->fetch_assoc();
  $imm = $row["PathImmagine"];
}
$destination = $dir . $imm;
$query = "UPDATE prodotti SET Nome='$nome', Categoria='$categoria', Prezzo='$price', PathImmagine='$imm', Disp='$disp' WHERE Username='$usernameFornitore' AND Nome='$nomeLast'";
$result = $conn->query($query);
if ($result === FALSE) {
  echo "errore inserimento tabella di login";
}

move_uploaded_file($fileTmp, $destination);
$conn->close();
setcookie("prodotto", $nomeLast, time()-1);
header("location: ../fornitore/index_forn.php");

?>
