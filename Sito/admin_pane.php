<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
$queryFornitori = "SELECT Username, Negozio FROM register WHERE Negozio IS NOT NULL";
$queryUtenti = "SELECT Username, Nome, Cognome FROM register WHERE Negozio IS NULL";
$queryCosto = "SELECT * FROM spedizione";
?>





<!DOCTYPE html>
<html lang="it-IT" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Pannello amministrativo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="./css/general.css">
    <link rel="stylesheet" href="./css/header_nav.css">
    <link rel="stylesheet" href="./css/admin.css">
  </head>
  <body>
    <header>
      <a href="./index.php">
        <figure>
          <img id="logo" src="./img/logo.png" alt="logo sito"/>
        </figure>
      </a>
      <div id="navExtended">
          <div><span><a href="#">Elimina utente</a></span></div>
          <div class="opac"><span><a href="#">Elenco fornitori</a></span></div>
          <div><span><a href="#">Elenco utenti</a></span></div>
          <div><span><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></span></div>
      </div>
      <div class="container-toggle">
        <div class="toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </div>
      </div>
    </header>
    <nav id="nav-collapsed">
      <ul>
        <li class="opac"><a href="#">Elimina utente</a></li>
        <li><a href="#">Elenco fornitori</a></li>
        <li  class="opac"><a href="./modify_product.php">Elenco utenti</a></li>
        <li><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></li>
      </ul>
    </nav>
    <section class="container-principal">
      <h1>Pannello di amministrazione</h1>
      <section class="container-section">

        <section class="cont-mod">
          <section class="inner-section delete-utente">
            <h2>Elimina utente</h2>
            <div class="cont-input">
              <label for="user2delete">Username</label>
              <input id="user2delete" type="text" name="user2delete" value=""/>
            </div>
            <button class="button btn_delete" type="button" onclick="elimina_utente()" name="btn_delete">Elimina</button>
          </section>
          <section class="inner-section delivery-utente">
            <h2>Dati spedizione</h2>
            <?php
            $result = $conn->query($queryCosto);
            $row = $result->fetch_assoc();
            ?>
            <span>Attuale costo di spedizione: <?php echo $row["costo"]; ?>€</span>
            <div class="cont-input">
              <label for="delivery">Costo di spedizione</label>
              <input id="delivery" type="text" name="delivery" value=""/>
            </div>
            <button class="button btn_modify" type="button" name="btn_modify" onclick="modifica_spedizione(<?php echo $row["costo"]; ?>)">Modifica</button>
          </section>
        </section>

        <section class="cont-list">
          <section class="inner-section-cat list-utente">
            <section class="category">
              <div class="category-title">
                <div class="container-title">
                  <h2>Elenco fornitori</h2>
                </div>
                <div class="container-icon plus">
                  <span class="fas fa-plus"></span>
                </div>
                <div class="container-icon minum">
                  <span class="fas fa-minus"></span>
                </div>
              </div>
            </section>
            <section class="list">
              <div class="cont-forn cont-header">
                <h3 class="user userHeader">Username</h3>
                <h3 class="nameRest nameHeader">Nome Ristorante</h3>
              </div>
              <?php
              $result = $conn->query($queryFornitori);
                while ($row = $result->fetch_assoc()) {
              ?>
              <div class="cont-forn">
                <span class="user"><?php echo $row["Username"] ?></span>
                <span class="nameRest"><?php echo $row["Negozio"] ?></span>
              </div>
              <?php
                }
              ?>
            </section>
          </section>
          <section class="inner-section-cat list-utente">
            <section class="category">
              <div class="category-title">
                <div class="container-title">
                  <h2>Elenco utenti</h2>
                </div>
                <div class="container-icon plus">
                  <span class="fas fa-plus"></span>
                </div>
                <div class="container-icon minum">
                  <span class="fas fa-minus"></span>
                </div>
              </div>
            </section>
            <section class="list">
              <div class="cont-user cont-header">
                <h3 class="user userHeader">Username</h3>
                <h3 class="name nameHeader">Nome</h3>
                <h3 class="surname surnameHeader">Cognome</h3>
              </div>
              <?php
              $result = $conn->query($queryUtenti);
                while ($row = $result->fetch_assoc()) {
              ?>
              <div class="cont-user">
                <span class="user"><?php echo $row["Username"] ?></span>
                <span class="name"><?php echo $row["Nome"] ?></span>
                <span class="surname"><?php echo $row["Cognome"] ?></span>
              </div>
              <?php
                }
              ?>
            </section>
          </section>
        </section>
      </section>
    </section>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="./js/scrollNav.js"></script>
    <script src="./js/admin.js"></script>
  </body>
</html>
