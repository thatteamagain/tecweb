$(function() {

  //SET TOTAL FOREACH PRODUCT
  $(".quantity").each(function( i ) {
    var indexQta = $(".quantity").index(this);
    var quantity = $(".quantity").eq(indexQta).val();
    var prezzo = $(".price").eq(indexQta).text();
    prezzo = prezzo.replace(",", ".");
    number = prezzo * quantity;
    number = Number(number).toFixed(2);
    $(".total").eq(indexQta).text(number);
  });

  $(".final_total").each(function( i ) {
    var indexTotal = $(".final_total").index(this);
    var totalProdotti = 0;
    var price;
    var length = $(".container-products").eq(indexTotal).find(".price").length;

    for(var j = 0; j < length; j++) {
      price = $(".container-products").eq(indexTotal).find(".total").eq(j).html();
      price = price.replace(/,/,".");
      totalProdotti = (+totalProdotti) + (+price);
    }

    totalProdotti = parseFloat(totalProdotti).toFixed(2);
    $(".totalProdotti").eq(indexTotal).html(totalProdotti);

    var totSpedizione = $(".totalSpedizione").eq(indexTotal).html();
    totSpedizione = totSpedizione.replace(/,/,".");

    var totOrdine = (+totalProdotti) + (+totSpedizione);
    totOrdine = parseFloat(totOrdine).toFixed(2);
    $(".totalOrdine").eq(indexTotal).html(totOrdine);

    if ($(window).width() >= 1024) {
      // console.log($(".container-products").eq(i).find(".row-product").length);
      if($(".container-products").eq(i).find(".row-product").length % 3 == 2) {
        $(this).addClass("final_total_third");
      }
      if($(".container-products").eq(i).find(".row-product").length % 3 == 1) {
        $(this).addClass("final_total_twothird");
      }
    } else if ($(window).width() >= 768) {
      if($(".container-products").eq(i).find(".row-product").length % 2 != 0) {
        $(this).addClass("final_total_half");
      }
    }
  });


});

$(".btn_mod_qta").click( function() {
  var indexBtn = $(".btn_mod_qta").index(this);
  $(".quantity").eq(indexBtn).prop("disabled", false);
  $(".btn_mod_qta").eq(indexBtn).css("display", "none");
  $(".btn_conf_qta").eq(indexBtn).css("display", "block");
})

$(".btn_conf_qta").click( function() {
  var indexBtn = $(".btn_conf_qta").index(this);
  $(".quantity").eq(indexBtn).prop("disabled", true);
  $(".btn_conf_qta").eq(indexBtn).css("display", "none");
  $(".btn_mod_qta").eq(indexBtn).css("display", "block");
})


function modifica_quantita(idOrdine, nomeProdotto, tag) {
  quantita = $(tag).prev().prev().val();
  $.ajax({
    type: "POST",
    url:"../php/modifica_quantita_prodotto.php",
    data: "IDorder=" + idOrdine + "&nomeProdotto=" + nomeProdotto + "&quantita=" + quantita,
    success: function(result) {
      console.log(result);
    },
    error: function(dati){
      console.log(dati.responseText);
    }
  });
  window.location.assign("../utente/ordini.php");
}

function elimina_ordine(idOrdine) {
  $.ajax({
    type: "POST",
    url:"../php/elimina_ordine.php",
    data: "IDorder=" + idOrdine,
    success: function(result) {
      console.log(result);
    },
    error: function(dati){
      console.log(dati.responseText);
    }
  });
  document.cookie = "ID_ordine=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
  document.cookie = "ProductQuantity=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
  window.location.assign("../utente/elenco_fornitori.php");
}
