function fadeInConfirm() {
  $("#confirmDialog").fadeIn();
  $("#confirmDialog").css("display", "flex");
}

function fadeOutConfirm() {
  $("#confirmDialog").fadeOut();
}
