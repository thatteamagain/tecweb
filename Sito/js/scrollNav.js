var cont = 0;
var go = false;

$( document ).ready(function() {
  $(".toggle").click( function() {
    //console.log("ciao");
    if(!go) {
      go = true;
      $("#nav-collapsed").slideDown(600);
    } else {
      $("#nav-collapsed").slideUp(600);
      go = false;
    }
  });

  //scorrimento con nav aperta da evitare
  $(window).scroll( function(e) {
    if ($("#nav-collapsed").is(":visible")) {
      $("#nav-collapsed").slideUp();
      go = false;
    }
  });
});
