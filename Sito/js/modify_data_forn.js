//add new img display immediatly

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#img_new_forn').attr('src', e.target.result).width("18rem").height("14rem");
    };
    reader.readAsDataURL(input.files[0]);
  }
}

//fade out add new file (img)

function fade() {
  $(".none").fadeOut();
}

//able textbox and modify btn

$(function() {
  $(".row_mod>.fas").click( function() {
    var indexI = $(".row_mod>.fas").index(this);
    if(indexI == 0) {       //se è la prima icona allora faccio apparire la schermata di inserimento img
      $(".none").fadeIn();
    } else if(indexI == 4) {  //l'ultima icona serve per far abilitare tutti i number input
      $(".contHourClose > input").prop('readonly', false);
      $(".contHourClose > input").css('box-shadow', "0px 0px 2px #ffa500");
      $(".contHourOpen > input").prop('readonly', false);
      $(".contHourOpen > input").css('box-shadow', "0px 0px 2px #ffa500");
    } else {                  //in tutti gli altri casi abilito i rispettivi input
      $(".row_mod > div > input").eq(indexI-1).prop('readonly', false);
      $(".row_mod > div > input").eq(indexI-1).css('box-shadow', "0px 0px 2px #ffa500");
    }
    $(".right").css("background-color", "rgba(0, 255, 0, 0.6)");
  })
});

function open_script(){
   window.location.assign('../php/delete_forn.php');//there are many ways to do this
}
