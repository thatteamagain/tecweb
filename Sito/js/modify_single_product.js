$(function() {
  $(".row_mod>.fas").click( function() {
    var indexI = $(".row_mod>.fas").index(this);
    if(indexI == 0) {       //se è la prima icona allora faccio apparire la schermata di inserimento img
      $(".none").fadeIn();
    } else if(indexI == 2) {  //l'ultima icona serve per far abilitare tutti i number input
      $(".cont_cat > select").prop('disabled', false);
      $(".cont_cat > select").css('box-shadow', "0px 0px 2px #ffa500");

    } else if(indexI == 4) {
      $(".cont_disp > select").prop('disabled', false);
      $(".cont_disp > select").css('box-shadow', "0px 0px 2px #ffa500");
    } else {                  //in tutti gli altri casi abilito i rispettivi input
      if(indexI < 3) {
        $(".row_mod > div > input").eq(indexI-1).prop('readonly', false);
        $(".row_mod > div > input").eq(indexI-1).css('box-shadow', "0px 0px 2px #ffa500");
      } else {
        $(".row_mod > div > input").eq(indexI).prop('readonly', false);
        $(".row_mod > div > input").eq(indexI).css('box-shadow', "0px 0px 2px #ffa500");
      }

    }
    $(".right").css("background-color", "rgba(0, 255, 0, 0.6)");
    $(".right").prop('disabled', false);
  });

//able new cat input
  $(".cont_cat > select").change( function() {
    if(this.value == "new") {
      $("#newCat").prop("disabled", false);
      $("#newCat").css('box-shadow', "0px 0px 2px #ffa500");
    }
    $(".cont_cat > input").val(this.value) ;
  });


  $(".cont_disp > select").change( function() {
    $(".cont_disp > input").val(this.value) ;
  });

});

/*function open_script(){
   window.location.assign('../php/modify_one_product.php');//there are many ways to do this
}*/

//fade out add new file (img)

function fade() {
  $(".none").fadeOut();
}
