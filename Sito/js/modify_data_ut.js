function open_script(){
   window.location.assign('../php/delete_ut.php');//there are many ways to do this
}

$(function() {
  $(".row_mod>.fas").click( function() {
    var indexI = $(".row_mod>.fas").index(this);
    $(".row_mod > div > input").eq(indexI).prop('readonly', false);
    $(".row_mod > div > input").eq(indexI).css('box-shadow', "0px 0px 2px #ffa500");
    $(".right").css("background-color", "rgba(0, 255, 0, 0.6)");
    $(".right").prop('disabled', false);
    if(indexI == 2) {
      $(".row_mod > div > input").eq(indexI+1).css('box-shadow', "0px 0px 2px #ffa500");
      $(".row_mod > div > input").eq(indexI+1).prop('readonly', false);
    }
  })
});
