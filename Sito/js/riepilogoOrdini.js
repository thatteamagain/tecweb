$(function() {

  $(".down").click( function() {
    var i = $(".down").index(this);
    if($(".down").eq(i).is(":visible")) {
      $(".down").eq(i).hide();
      $(".up").eq(i).css("display", "flex");
      $(".listOrdini").eq(i).slideDown();
    }
  });
  $(".up").click( function() {
    var i = $(".up").index(this);
    if($(".up").eq(i).is(":visible")) {
      $(".up").eq(i).hide();
      $(".down").eq(i).css("display", "flex");
      $(".listOrdini").eq(i).slideUp();
    }
  });

  $(".down-sub").click( function() {
    var i = $(".down-sub").index(this);
    if($(".down-sub").eq(i).is(":visible")) {
      $(".down-sub").eq(i).hide();
      $(".up-sub").eq(i).css("display", "flex");
      $(".listProdotti").eq(i).slideDown();
    }
  });

  $(".up-sub").click( function() {
    var i = $(".up-sub").index(this);
    if($(".up-sub").eq(i).is(":visible")) {
      $(".up-sub").eq(i).hide();
      $(".down-sub").eq(i).css("display", "flex");
      $(".listProdotti").eq(i).slideUp();
    }
  });
});

function modifica_stato(idOrd) {
  console.log(idOrd);
  $.ajax({
      type: "POST",
      url:"../php/ship.php",
      data: "ID_ordine=" + idOrd,
      success: function(result) {
      },
      error: function(data){
        alert(data.responseText);
      }
    });
    document.cookie = "OrdineShip=" + idOrd + "; path=/;";
  window.location.assign("../php/email_ship.php");
  //window.location.assign("../fornitore/riepilogoOrdini.php");
}
