var clickCarrello = 0;
var length = 0;
$(function() {
  $(window).scroll( function(e) {
    if ($(window).scrollTop() > $("header").height()) {
      $(".chart").css("position", "fixed");
      $(".chart").css("top", "0");
    } else {
      $(".chart").css("position", "absolute");
      $(".chart").css("top", $("header").height());
    }
  });

  $("#carrello").click( function() {
    $(".chart").toggle("slide");
    if(clickCarrello % 2 == 0) {
      $("footer").css("opacity", "0.2");
      $("#container-products").css("opacity", "0.2");
      $("#container-section").css("opacity", "0.2");
      $("#cont-info-forn").css("opacity", "0.2");
    } else {
      $("footer").css("opacity", "1");
      $("#container-products").css("opacity", "1");
      $("#container-section").css("opacity", "1");
      $("#cont-info-forn").css("opacity", "1");
    }
    clickCarrello++;
  });

  setInterval(function(){ createChart(); }, 1000);

});

function createChart() {
  s = getProductQuantity();
  var i = 0;
  if(s != "Cookie non settato") {
    var completeCookie = s;
    // console.log(completeCookie);
    //var dataCookie = completeCookie.substr(16);
    // console.log(dataCookie);
    rows = getRowPQ(completeCookie);

    var products = new Array();
    var quantity = new Array();
    var path = new Array();

    for(i = 1; i < rows.length-1; i++) {
      // console.log("rows i");
      // console.log(rows[i]);
      singleRow = rows[i].split("~");

      // console.log("single row");
      // console.log(singleRow[1]);
      products.push(singleRow[0]);
      quantity.push(singleRow[1]);
      path.push(singleRow[2]);
    }

    var currentLength = products.length;

    if(length != currentLength) {
      length = currentLength;

      $(".chart").empty();
      for(i = 0; i < length; i++) {
        createChartElement(products, quantity, i, path);
      }
      $(".chart").append("<div class='contLink'></div>");
      $(".contLink").append("<a href='./ordini.php' class='linkCarrello'>Procedi all'ordine</span>");
      setImgChart(length);
    }
  }
}

function setImgChart(len) {
  $(".contNumber").remove();
  $(".right_figure").append("<div class='contNumber'></div>");
  $(".contNumber").append("<span></span>");
  $(".contNumber>span").append(len);
}

function createChartElement(products, quantity, i, path) {
  var imm = "../img/prodotti/" + path[i];
  var stringa = "<img src='" + imm + "' alt='immagine prodotto nel carrello' class='ImgChar'/>"
  $(".chart").append("<div class='chartElement'></div>");
  $(".chartElement").eq(i).append("<div class='contImgChar'></div>");
  $(".contImgChar").eq(i).append("<figure class='figureImgChar'></figure>");
  $(".figureImgChar").eq(i).append(stringa);

  $(".chartElement").eq(i).append("<div class='contInfochart'></div>");
  $(".contInfochart").eq(i).append("<span>Prodotto</span>");
  $(".contInfochart").eq(i).append("<span class='title-forn chartProduct'></span>");
  $(".chartProduct").eq(i).append(products[i]);
  $(".contInfochart").eq(i).append("<span>Quantit&agrave;</span>");
  $(".contInfochart").eq(i).append("<span class='title-forn chartQta'></span>");
  $(".chartQta").eq(i).append(quantity[i]);

}

function getRowPQ(s) {
  rows = s.split("§");
  return rows;
}

function getProductQuantity() {
  //cc = document.cookie.split(";");
  /*for(var i = 0; i < cc.length; i++) {
    if(cc[i].includes("ProductQuantity")) {
      alert(getCookie[cc[i]]);
      return getCookie[cc[i]];
    }
  }*/

  if (isset("ProductQuantity")) {
    return getCookie("ProductQuantity");
  }
  return "Cookie non settato";

}


function aggiungi(nomeProdotto, fornitore, utente, quantity) {
  quantita = $(quantity).prev().val();
  $.ajax({
    type: "POST",
    url:"../php/add_product_to_order.php",
    data: "nomeProdotto=" + nomeProdotto + "&nomeFornitore=" + fornitore + "&nomeUtente=" + utente + "&quantita=" + quantita,
    success: function(result) {
      // console.log(result);
      document.cookie = result;
    },
    error: function(dati){
      //console.log(dati.responseText);
      $(".cont_dialog>span").empty();

      $("#confirmDialog").fadeIn();
      $("#confirmDialog").css("display", "flex");
      $(".cont_dialog>span").append(dati.responseText);
    }
  });

}

function fadeInConfirm() {
  $("#confirmDialog").fadeIn();
  $("#confirmDialog").css("display", "flex");
}

function isset(name)
{
    var re = new RegExp(name + "=([^;]+)");
    var value = re.exec(document.cookie);
    return (value != null) ? unescape(value[1]) : null;
}


function getCookie(c_name)
{
    var i,x,y,ARRcookies=document.cookie.split(";");

    for (i=0;i<ARRcookies.length;i++)
    {
        x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
        y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
        x=x.replace(/^\s+|\s+$/g,"");
        if (x==c_name)
        {
            return unescape(y);
        }
     }
}
