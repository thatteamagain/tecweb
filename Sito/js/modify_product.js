$(function() {

  $(".modify_btn_cat").click( function() {
    if ($(window).width() >= 1024) {
      var indexNewCat = $(".modify_btn_cat").index(this);
      if($(".modify_category_large").eq(indexNewCat).is(":visible")) {
        $(".modify_category_large").eq(indexNewCat).slideUp();
      } else {
        $(".modify_category_large").eq(indexNewCat).slideDown();
        $(".modify_category_large").eq(indexNewCat).css("display", "flex");
      }
    } else {
      var indexNewCat = $(".modify_btn_cat").index(this);
      if($(".modify_category").eq(indexNewCat).is(":visible")) {
        $(".modify_category").eq(indexNewCat).slideUp();
      } else {
        $(".modify_category").eq(indexNewCat).slideDown();
        $(".modify_category").eq(indexNewCat).css("display", "flex");
      }
    }
  });

  //MODIFICA NOME CATEGORIA
  $(".setNewCat").click( function() {
    if ($(window).width() >= 1024) {
      var i = $(".setNewCat").index(this);
      $(".modify_category_large").eq(i).slideUp();
      var newNameCat = $(".newCat").val();
      $(".container-title > h2").eq(i+1).html(newNameCat);
      $(".newCat").val("");
    } else {
      var i = $(".setNewCat").index(this);
      $(".modify_category").eq(i).slideUp();
      var newNameCat = $(".newCat").val();
      $(".container-title > h2").eq(i+1).html(newNameCat);
      $(".newCat").val("");
    }
  });

  $(".container-products").each(function( i ) {
    if ($(window).width() >= 1024) {
      if($(this).find(".row-product").length % 3 != 0) {
        $(this).css("border-right", "2px solid #ffa500");
        $(this).find(".row-product").css("border-bottom", "0px");
        $(this).css("border-bottom", "2px solid #ffa500");
      }
    } else if ($(window).width() >= 768) {
      if($(this).find(".row-product").length % 2 != 0) {
        $(this).css("border-right", "2px solid #ffa500");
        $(this).find(".row-product").css("border-bottom", "0px");
        $(this).css("border-bottom", "2px solid #ffa500");
      }
   }
 });
});
