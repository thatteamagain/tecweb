$(function() {

/*OPEN AND CLOSE CATEGORIES*/
  $(".plus").click( function() {
    var i = $(".plus").index(this);
    if($(".plus").eq(i).is(":visible")) {
      $(".plus").eq(i).hide();
      $(".minum").eq(i).css("display", "flex");
      $(".list").eq(i).slideDown(600);
    }
  });
  $(".minum").click( function() {
    var i = $(".minum").index(this);
    if($(".minum").eq(i).is(":visible")) {
      $(".minum").eq(i).hide();
      $(".plus").eq(i).css("display", "flex");
      $(".list").eq(i).slideUp(600);
    }
  });
});

function modifica_spedizione(costoPrecedente) {
  document.cookie = "vecchio=" + costoPrecedente + ";path='/'";
  document.cookie = "nuovo=" + $("#delivery").val() + ";path='/'";

  window.location.assign("./php/modify_shipping.php");

}

function elimina_utente() {
  document.cookie = "user_deleted=" + $("#user2delete").val() + ";path='/'";
  window.location.assign("./php/delete_user_admin.php");
}
