var current = 0;

var usernameUsedUser;
var usernameUsedForn;
var emailUsedUser;
var emailUsedForn;

function myFunction1(var1, var2) {
  var array = var1.split("?");
  usernameUsedUser = false;
  for (var i = 0; i < array.length; i++) {
    if (array[i] === var2) {
      usernameUsedUser = true;
    }
  }
  if (usernameUsedUser === true && emailUsedUser === true) {
    $("#cred_used_user").html("Username e email già utilizzati");
    $("#cred_used_user").css("display","block");
  } else if (usernameUsedUser === true) {
    $("#cred_used_user").html("Username già utilizzato");
    $("#cred_used_user").css("display","block");
  } else if (emailUsedUser === true) {
      $("#cred_used_user").html("Email già utilizzata");
      $("#cred_used_user").css("display","block");
      usernameUsedUser = false;
    } else {
    $("#cred_used_user").html("");
    $("#cred_used_user").css("display","none");
    emailUsedUser = false;
    usernameUsedUser = false;
  }
  if(usernameUsedUser === true || emailUsedUser === true) {
    $('#registration-submit-user').prop('disabled', true);
  } else {
    $('#registration-submit-user').prop('disabled', false);
  }
};

function myFunction2(var1, var2) {
  var array = var1.split("?");
  emailUsedUser = false;
  for (var i = 0; i < array.length; i++) {
    if (array[i] === var2) {
      emailUsedUser = true;
    }
  }
  if (usernameUsedUser === true && emailUsedUser === true) {
    $("#cred_used_user").html("Username e email già utilizzati");
    $("#cred_used_user").css("display","block");
  } else if (emailUsedUser === true) {
    $("#cred_used_user").html("Email già utilizzata");
    $("#cred_used_user").css("display","block");
  }  else if (usernameUsedUser === true) {
    $("#cred_used_user").html("Username già utilizzato");
    $("#cred_used_user").css("display","block");
    emailUsedUser = false;
  } else {
    $("#cred_used_user").html("");
    $("#cred_used_user").css("display","none");
    emailUsedUser = false;
    usernameUsedUser = false;
  }
  if(emailUsedUser === true || usernameUsedUser === true) {
    $('#registration-submit-user').prop('disabled', true);
  } else {
    $('#registration-submit-user').prop('disabled', false);
  }
};

function myFunction3(var1, var2) {
  var array = var1.split("?");
  usernameUsedForn = false;
  for (var i = 0; i < array.length; i++) {
    if (array[i] === var2) {
      usernameUsedForn = true;
    }
  }
  if (usernameUsedForn === true && emailUsedForn === true) {
    $("#cred_used_forn").html("Username e email già utilizzati");
      $("#cred_used_user").css("display","block");
  } else if (usernameUsedForn === true) {
    $("#cred_used_forn").html("Username già utilizzato");
      $("#cred_used_user").css("display","block");
  } else if (emailUsedForn === true) {
      $("#cred_used_forn").html("Email già utilizzata");
        $("#cred_used_user").css("display","block");
      usernameUsedForn = false;
    } else {
    $("#cred_used_forn").html("");
    $("#cred_used_user").css("display","none");
    usernameUsedForn = false;
    emailUsedForn = false;
  }
  if(usernameUsedForn === true || emailUsedForn === true) {
    $('#registration-submit-forn').prop('disabled', true);
  } else {
    $('#registration-submit-forn').prop('disabled', false);
  }
};
function myFunction4(var1, var2) {
  var array = var1.split("?");
  emailUsedForn = false;
  for (var i = 0; i < array.length; i++) {
    if (array[i] === var2) {
      emailUsedForn = true;
    }
  }
  if (usernameUsedForn === true && emailUsedForn === true) {
    $("#cred_used_forn").html("Username e email già utilizzati");
      $("#cred_used_user").css("display","block");
  } else if (emailUsedForn === true) {
    $("#cred_used_forn").html("Email già utilizzata");
      $("#cred_used_user").css("display","block");
  }  else if (usernameUsedForn === true) {
    $("#cred_used_forn").html("Username già utilizzato");
      $("#cred_used_user").css("display","block");
    emailUsedForn = false;
  } else {
    $("#cred_used_forn").html("");
    $("#cred_used_user").css("display","none");
    usernameUsedForn = false;
    emailUsedForn = false;
  }
  if(usernameUsedForn === true || emailUsedForn === true) {
    $('#registration-submit-forn').prop('disabled', true);
  } else {
    $('#registration-submit-forn').prop('disabled', false);
  }
};

$(function() {

  window.onorientationchange = function() {
    window.location.reload();
  };

  $(function() {
    var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    $("html").css({"width":w,"height":h});
    $("body").css({"width":w,"height":h});
  });

  $(".login-window").click( function() {
    if(current != 0) {
      current = 0;
      $(".login-information").css("display", "block");
      $(".reg-information").css("display", "none");
      $(".reg-window").css("border-left", "0px");
      $(".reg-window").css("border-bottom", "1px solid black");
      $(".reg-window").css("border-top-left-radius", "0px");

      $(".login-window").css("border-right", "1px solid black");
      $(".login-window").css("border-bottom", "0px");
      $(".login-window").css("border-top-right-radius", "15px");
    }
  });

  $(".reg-window").click( function() {
    if(current != 1) {
      current = 1;
      $(".reg-information").css("display", "flex");
      $(".login-information").css("display", "none");
      $(".login-window").css("border-right", "0px");
      $(".login-window").css("border-bottom", "1px solid black");
      $(".login-window").css("border-top-right-radius", "0px");
      $(".reg-window").css("border-left", "1px solid black");
      $(".reg-window").css("border-bottom", "0px");
      $(".reg-window").css("border-top-left-radius", "15px");
    }
  });

  $('input:radio[name="type"]').change(
    function(){
        if ($(this).is(':checked') && $(this).val() == 'acq') {
          $(".reg-information").css("display", "flex");
          $(".reg-information").css("align-items", "center");
            $(".user").css("display","block");
            $(".forn").css("display","none");
        }
        if ($(this).is(':checked') && $(this).val() == 'forn') {
            $(".reg-information").css("display", "block");
            $(".reg-information").css("align-items", "normal");
            $(".user").css("display","none");
            $(".forn").css("display","block");
        }
    });
});
