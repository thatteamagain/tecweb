function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

$(function() {
  var columns = $(".container>a");
  for(var i = 0; i < columns.length; i++) {
    if(i%2 != 0) {
      $(columns).eq(i).css("margin-left", "0");
    }
  }
});
