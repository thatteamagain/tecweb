$(function() {

/*OPEN AND CLOSE CATEGORIES*/
  $(".plus").click( function() {
    var i = $(".plus").index(this);
    if($(".plus").eq(i).is(":visible")) {
      $(".plus").eq(i).hide();
      $(".minum").eq(i).css("display", "flex");
      $(".container-products").eq(i).slideDown(600);
    }
  });
  $(".minum").click( function() {
    var i = $(".minum").index(this);
    if($(".minum").eq(i).is(":visible")) {
      $(".minum").eq(i).hide();
      $(".plus").eq(i).css("display", "flex");
      $(".container-products").eq(i).slideUp(600);
    }
  });

  /*CALCULATE TOTAL IN PRODUCT*/

  $(".quantity").change( function() {
    var indexQta = $(".quantity").index(this);
    var quantity = $(".quantity").eq(indexQta).val();
    var prezzo = $(".price").eq(indexQta).text();
    prezzo = prezzo.replace(",", ".");
    number = prezzo * quantity;
    number = Number(number).toFixed(2);
    $(".total").eq(indexQta).text(number);
  });

  /*RESET VALUE TOTAL AND QUANTITY*/
  $(".add-chart").click( function() {
    var indexBtn = $(".add-chart").index(this);
    $(".total").eq(indexBtn).text("0.00");
    $(".quantity").eq(indexBtn).val(0);
  });
});
