function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      $('#imgP').attr('src', e.target.result).width("18rem").height("14rem");
    };
    reader.readAsDataURL(input.files[0]);
  }
}

$(function() {
  $("#catProduct").change( function() {
    if($(this).val() === "new") {
      $("#newCat").prop("disabled", false);
    } else {
      $("#newCat").prop("disabled", true);
    }
  });
});
