$(function() {

  $(".down").click( function() {
    var i = $(".down").index(this);
    if($(".down").eq(i).is(":visible")) {
      $(".down").eq(i).hide();
      $(".up").eq(i).css("display", "flex");
      if(i == 0) {
        $(".subcategories").slideDown();
      } else {
        $(".container-products").eq(i-1).slideDown(600);
      }
    }
  });
  $(".up").click( function() {
    var i = $(".up").index(this);
    if($(".up").eq(i).is(":visible")) {
      $(".up").eq(i).hide();
      $(".down").eq(i).css("display", "flex");
      if(i == 0) {
        $(".subcategories").slideUp();
      } else {
        $(".container-products").eq(i-1).slideUp(600);
      }
    }
  });
});
