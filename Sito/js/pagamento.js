$(function() {
  $('input:radio[name="radioPagamento"]').change(
    function(){
        if ($(this).is(':checked') && $(this).val() == 'card') {
          $(".cont_dataCard").css("display", "block");
          $(".singleInput > input").prop("required", true);
        } else {
          $(".cont_dataCard").css("display", "none");
          $(".singleInput > input").prop("required", false);
        }
  });
})
