<?php
/* LEGENDA ORDINI */
// INCOMPLETO
// ULTIMATO
// PAGATO o DA PAGARE
// SPEDITO

$incrementalID = 0;
$check = true;

$usernameUser = $_COOKIE["user"];
if(isset($_COOKIE["fornitore"])) {
  $usernameFornitore = $_COOKIE["fornitore"];
  $check = false;
}


$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
if (isset($_COOKIE["ID_ordine"])) {
  $idOrdine = $_COOKIE["ID_ordine"];
  $query = "UPDATE storico SET Orario=NULL, Luogo=NULL, Stato='incompleto' WHERE ID_ordine='$idOrdine'";
  $result = $conn->query($query);
}


$query = "SELECT COUNT(ID_ordine) AS tupleOrdine FROM storico WHERE Utente='$usernameUser' AND Stato='incompleto'";
$result = $conn->query($query);
$row = $result->fetch_assoc();
?>




<!DOCTYPE html>
<html lang="it-IT" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Carrello riepilogo ordini</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/header_nav.css">
    <link rel="stylesheet" href="../css/view_prodotti.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link rel="stylesheet" href="../css/ordini.css">
  </head>
  <body>
    <header>
      <a href="./elenco_fornitori.php">
        <figure>
          <img id="logo" src="../img/logo.png" alt="logo sito"/>
        </figure>
      </a>
      <div id="navExtended">
          <div class="opac"><span><a href="./elenco_fornitori.php">Home</a></span></div>
          <div><span><a href="./modify_data_ut.php">Modifica account</a></span></div>
          <div class="opac"><span><a href="./ordini.php">Riepilogo ordini</a></span></div>
          <div><span><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></span></div>
      </div>
      <div class="container-toggle">
        <div class="toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </div>
      </div>
    </header>
    <nav id="nav-collapsed">
      <ul>
        <li class="opac"><a href="./index_forn.php">Home</a></li>
        <li><a href="./modify_data_ut.php">Modifica account</a></li>
        <li  class="opac"><a href="./ordini.php">Riepilogo ordini</a></li>
        <li><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></li>
      </ul>
    </nav>
    <section id="container-products" class="toOpac">
      <h1>Riepilogo ordini</h1>
      <?php
      if($row["tupleOrdine"] > 0) {
        $query = "SELECT * FROM storico WHERE Utente='$usernameUser' AND Stato='incompleto'";
        $result = $conn->query($query);
      ?>
      <section class="category">
        <div class="category-title">
          <div class="container-title">
            <?php
              $queryFornitore = "SELECT Negozio FROM register WHERE Username='$usernameFornitore'";
              $resultF = $conn->query($queryFornitore);
              $rowF = $resultF->fetch_assoc();
            ?>
            <h2>Ordine in attesa - <?php echo $rowF["Negozio"]; ?></h2>
          </div>
          <div class="container-icon plus">
            <span class="fas fa-plus"></span>
          </div>
          <div class="container-icon minum">
            <span class="fas fa-minus"></span>
          </div>
        </div>
      </section>
      <div class="container-products">
        <?php
        while($row = $result->fetch_assoc()) {
        ?>
        <section class="row-product">
          <div class="container-information">
            <div class="cont-img-product">
              <figure>
                <?php
                $prodotto = $row["Prodotto"];
                $query2 = "SELECT * FROM prodotti WHERE Username='$usernameFornitore' AND Nome='$prodotto'";
                $result2 = $conn->query($query2);
                $row2 = $result2->fetch_assoc();
                $nome = $row2["Nome"];
                $pathImmagine = "../img/prodotti/" . $row2["PathImmagine"];
                ?>
                <img class="img-product" src="<?php echo $pathImmagine ?>" alt="immagine prodotto">
              </figure>
            </div>
            <div class="information-product">
              <span class="title-forn"><?php echo $row2["Nome"] ?> - &nbsp;<span class="price"><?php echo $row2["Prezzo"] ?></span>€</span>
              <span><?php echo $row2["Descrizione"] ?></span>
            </div>
          </div>
          <div class="buy-product">
            <label for="quantity<?php echo $incrementalID;?>">Quantità</label>
            <input id="quantity<?php echo $incrementalID; $incrementalID++;?>" class="quantity" type="number" name="quantity" value="<?php echo $row["Quantita"] ?>" min="0" disabled/>
            <button class="btn_mod_qta" type="button" name="buttonModifyQta">Modifica</button>
            <button class="btn_conf_qta" type="button" name="buttonConfirmQta" onclick="modifica_quantita('<?php echo $idOrdine; ?>', '<?php echo $nome; ?>', this)">Conferma</button>
            <span>Total: <span class="total">0.00</span>€</span>
          </div>
        </section>
        <?php
        }
        ?>
        <section class="final_total">
          <div class="cont_rowSpan">
            <span class="spanLeft">Totale prodotti</span>
            <span class="totalProdotti spanRight"></span>€
          </div>
          <div class="cont_rowSpan">
            <span class="spanLeft">Spedizione</span>
            <?php
              $queryS = "SELECT * FROM Spedizione";
              $resultS = $conn->query($queryS);
              $rowS = $resultS->fetch_assoc();
            ?>
            <span class="totalSpedizione spanRight"><?php echo $rowS["costo"]; ?></span>€
          </div>
          <div class="cont_rowSpan">
            <span class="spanLeft toBold">Totale ordine</span>
            <span class="toBold spanRight totalOrdine"></span>€
          </div>
          <div class="cont2pay">
            <button type="button" name="button" onclick="window.location.href='./spedizione.php'">Prosegui</button>
            <button type="button" name="button" onclick="elimina_ordine('<?php echo $idOrdine; ?>')">Elimina</button>
          </div>
        </section>
      </div>
      <?php
      }
      ?>



      <?php
      /* DA QUESTO PUNTO VERRANNO VISUALIZZATI TUTTI GLI ORDINI COMPLETATI */
      $queryOrdiniCompletati = "SELECT ID_ordine, Stato, Fornitore FROM storico WHERE Utente='$usernameUser' AND Stato<>'incompleto' GROUP BY ID_ordine"; /* questa query restituisce gli id degli ordini, lo stato e il fornitore con stato diverso da completato */
      $resultoc = $conn->query($queryOrdiniCompletati);
      while($rowoc = $resultoc->fetch_assoc()) {
        $idOrdine = $rowoc["ID_ordine"];
        $statoOrdine = $rowoc["Stato"];
        $usernameFornitore = $rowoc["Fornitore"];
      ?>
      <section class="category">
        <div class="category-title">
          <div class="container-title">
            <?php
            $query = "SELECT Negozio FROM register WHERE Username='$usernameFornitore'";
            $result = $conn->query($query);
            $row = $result->fetch_assoc();
            ?>
            <h2>Ordine id: <?php echo $idOrdine ?> - <?php echo $statoOrdine ?> - <?php echo $row["Negozio"] ?></h2>
          </div>
          <div class="container-icon plus">
            <span class="fas fa-plus"></span>
          </div>
          <div class="container-icon minum">
            <span class="fas fa-minus"></span>
          </div>
        </div>
      </section>
      <div class="container-products">

        <?php
        $queryOrdinazioni = "SELECT * FROM storico WHERE ID_ordine='$idOrdine'";
        $resulto = $conn->query($queryOrdinazioni);
        while($rowo = $resulto->fetch_assoc()) {
        $prod = $rowo["Prodotto"];
        $queryProdotto = "SELECT * FROM prodotti WHERE Username='$usernameFornitore' AND NOME='$prod'";
        $resultp = $conn->query($queryProdotto);
        $rowp = $resultp->fetch_assoc();
        ?>
        <section class="row-product">
          <div class="container-information">
            <div class="cont-img-product">
              <figure>
                <?php
                  $pathImmagine = "../img/prodotti/" . $rowp["PathImmagine"];
                ?>
                <img class="img-product" src="<?php echo $pathImmagine ?>" alt="immagine prodotto">
              </figure>
            </div>
            <div class="information-product">
              <span class="title-forn"><?php echo $rowp["Nome"]; ?> - &nbsp;<span class="price"><?php echo $rowp["Prezzo"]; ?></span>€</span>
              <span><?php echo $rowp["Descrizione"]; ?></span>
            </div>
          </div>
          <div class="buy-product">
            <label for="quantity<?php echo $incrementalID?>">Quantità</label>
            <input id="quantity<?php echo $incrementalID; $incrementalID++?>" class="quantity" type="number" name="quantity" value="<?php echo $rowo["Quantita"] ?>" disabled/>
            <!--<button class="btn_mod_qta" type="button" name="buttonModifyQta">Modifica</button> -->
            <span>Total: <span class="total">0.00</span>€</span>
          </div>
        </section>
        <?php
        }
        ?>


        <section class="final_total">
          <div class="cont_rowSpan">
            <span class="spanLeft">Totale prodotti</span>
            <span class="totalProdotti spanRight"></span>€
          </div>
          <div class="cont_rowSpan">
            <span class="spanLeft">Spedizione</span>
            <?php
            $queryCosto = "SELECT * FROM spedizione";
            $result = $conn->query($queryCosto);
            $row = $result->fetch_assoc();
            ?>
            <span class="totalSpedizione spanRight"><?php echo $row["costo"]; ?></span>€
          </div>
          <div class="cont_rowSpan">
            <span class="spanLeft toBold">Totale ordine</span>
            <span class="toBold spanRight totalOrdine"></span>€
          </div>

        </section>
      </div>
      <?php
        }
        $conn->close();
      ?>




    </section>

    <footer class="toOpac">
      <section class="subtitles-footer">
        <span class="title-forn">Contattaci</span>
        <div id="information">
          <span>Telefono: +39 123456789</span><span>Email: <a href="mailto:campus.gourmet.unibo@gmail.com?Subject=Help%20from%20website">campus.gourmet.unibo@gmail.com</a></span>
        </div>

      </section>
      <section class="subtitles-footer">
        <span class="title-forn">Tipologie di cucina</span>
        <div id="typologies">
          <span>PIADINERIA PIZZERIA HABURGERIA BIRRERIA SUPERMERCATI DISCOUNT CUCINA MEDITERRRANEA</span>
        </div>
      </section>
      <section class="subtitles-footer">
          <span class="title-forn">Su di noi</span>
          <p>Il nostro impegno è offrire un servizio vario e soddisfacente per tutti gli utenti del campus</p>
      </section>

    </footer>

    <!-- JQUERY E BOOTSTRAP JS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="../js/scrollNav.js"></script>
    <script src="../js/prodotti.js"></script>
    <script src="../js/ordini.js"></script>
  </body>
</html>
