<?php
$usernameUser = $_COOKIE["user"];

$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

/* prendo dalla tabella register nome e cognome dell'utente */
$queryRegister = "SELECT Nome, Cognome FROM register WHERE Username = \"$usernameUser\"";
$result = $conn->query($queryRegister);
$row = $result->fetch_assoc();
$nome = $row["Nome"];
$cognome = $row["Cognome"];

/* prendo dalla tabella delle credenziali la password dell'utente */
$queryCredenziali = "SELECT Pass FROM credenziali WHERE Username = \"$usernameUser\"";
$result = $conn->query($queryCredenziali);
$row = $result->fetch_assoc();
$pass = $row["Pass"];

$conn->close();
?>

<!DOCTYPE html>
<html lang="it-IT" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Modifica dati utente</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/header_nav.css">
    <link rel="stylesheet" href="../css/mod_data_forn.css">
    <link rel="stylesheet" href="../css/confirmDialog.css">
    <link rel="stylesheet" href="../css/mod_data_ut.css">
  </head>
  <body>
    <header>
      <a href="./elenco_fornitori.php">
        <figure>
          <img id="logo" src="../img/logo.png" alt="logo sito"/>
        </figure>
      </a>
      <div id="navExtended">
          <div class="opac"><span><a href="./elenco_fornitori.php">Home</a></span></div>
          <div><span><a href="./modify_data_ut.php">Modifica account</a></span></div>
          <div class="opac"><span><a href="./ordini.php">Riepilogo ordini</a></span></div>
          <div><span><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></span></div>
      </div>
      <div class="container-toggle">
        <div class="toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </div>
      </div>
    </header>
    <nav id="nav-collapsed">
      <ul>
        <li class="opac"><a href="./elenco_forn.php">Home</a></li>
        <li><a href="./modify_data_ut.php">Modifica dati</a></li>
        <li  class="opac"><a href="./ordini.php">Riepilogo ordini</a></li>
        <li><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></li>
      </ul>
    </nav>
    <section id="container-principal" class="all_content">
      <h1>Modifica account</h1>
      <section id="container_mod">
        <form class="form_modForn" action="../php/modifica_ut.php" method="post">
          <div class="row_mod">
            <div class="cont_name">
              <label class="labelMod" for="nameUt">Nome</label>
              <input class="inputMod" id="nameUt" type="text" name="name" placeholder="<?php echo $nome ?>" readonly/>
            </div>
            <span class="fas fa-edit row_i"></span>
          </div>
          <div class="row_mod">
            <div class="cont_surname">
              <label class="labelMod" for="surnameUt">Cognome</label>
              <input class="inputMod" id="surnameUt" type="text" name="surname" placeholder="<?php echo $cognome ?>" readonly/>
            </div>
            <span class="fas fa-edit row_i"></span>
          </div>
          <div class="row_mod">
            <div class="cont_pass">
              <label class="labelMod" for="passUt">Password</label>
              <input class="inputMod" id="passUt" type="password" name="pass" value="<?php echo $pass ?>" readonly/>
            </div>
            <span class="fas fa-edit row_i"></span>
          </div>
          <div class="row_mod">
            <div class="cont_pass">
              <label class="labelMod" for="pass2Ut">Conferma password</label>
              <input class="inputMod" id="pass2Ut" type="password" name="pass2" readonly/>
            </div>
          </div>
          <div class="row_mod">
            <div class="cont_btn">
              <button class="btn_forn" type="button" name="delete" onclick="fadeInConfirm()">Elimina Account</button>
              <input class="btn_forn right" type="submit" name="submit" value="Modifica" disabled/>
            </div>
          </div>
      </form>
      <div id="confirmDialog">
        <div class="cont_dialog">
          <span>Sei sicuro di voler eliminare l'account?</span>
          <div class="cont_btn_confirm">
            <button class="button left" type="button" name="buttonDeny" onclick="fadeOutConfirm()">Annulla</button>
            <button class="button right" type="button" name="buttonConfirm" onclick="open_script()">Conferma</button>
          </div>
        </div>
      </div>
      </section>
    </section>

  <!-- JQUERY E BOOTSTRAP JS-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="../js/scrollNav.js"></script>
  <script src="../js/modify_data_ut.js"></script>
  <script src="../js/confirmDialog.js"></script>
  </body>
</html>
