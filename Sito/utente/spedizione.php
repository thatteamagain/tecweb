<?php
/* LEGENDA ORDINI */
// INCOMPLETO
// ULTIMATO
// PAGATO o DA PAGARE
// SPEDITO

$usernameUser = $_COOKIE["user"];
$idOrdine = $_COOKIE["ID_ordine"];

$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$query = "SELECT Fornitore from storico WHERE ID_ordine='$idOrdine'";
$result = $conn->query($query);
$row = $result->fetch_assoc();
$forn = $row["Fornitore"];
$query2 = "SELECT Apertura, Chiusura FROM register WHERE Username='$forn'";
$result = $conn->query($query2);
$row = $result->fetch_assoc();
$ap = $row["Apertura"];
$ch = $row["Chiusura"];
$apertura = explode(":", $ap);
$chiusura = explode(":", $ch);
?>

<!DOCTYPE html>
<html lang="it-IT" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Spedizione</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/header_nav.css">
    <link rel="stylesheet" href="../css/spedizione.css">
  </head>
  <body>
    <header>
      <a href="./elenco_fornitori.php">
        <figure>
          <img id="logo" src="../img/logo.png" alt="logo sito"/>
        </figure>
      </a>
      <div id="navExtended">
          <div class="opac"><span><a href="./elenco_fornitori.php">Home</a></span></div>
          <div><span><a href="./modify_data_ut.php">Modifica account</a></span></div>
          <div class="opac"><span><a href="./ordini.php">Riepilogo ordini</a></span></div>
          <div><span><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></span></div>
      </div>
      <div class="container-toggle">
        <div class="toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </div>
      </div>
    </header>
    <nav id="nav-collapsed">
      <ul>
        <li class="opac"><a href="./elenco_fornitori.php">Home</a></li>
        <li><a href="./modify_data_ut.php">Modifica account</a></li>
        <li  class="opac"><a href="./ordini.php">Riepilogo ordini</a></li>
        <li><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></li>
      </ul>
    </nav>
    <section id="container_principal">
      <h1>Gestione spedizione</h1>
      <form class="" action="../php/database_spedizione.php" method="post">
        <div class="cont_luogo">
          <span>Seleziona il luogo di ritiro della spedizione</span>
          <div class="cont_radio">
            <fieldset>
              <div class="singleRadio">
                <input id="A" type="radio" name="radioSpedizione" value="Ingresso principale" required/>
                <label for="A">Ingresso principale</label>
              </div>
              <div class="singleRadio">
                <input id="B" type="radio" name="radioSpedizione" value="Entrata via Universita"/>
                <label for="B">Entrata via Universit&agrave;</label>
              </div>
            </fieldset>
          </div>
          <span>Scegli un orario di ritiro tra quelli disponibili</span>
          <div class="cont_radio">
            <div class="selectCont">
              <label for="ora">Orario</label>
              <?php
                $minuti = date('i');
                $ora = date('H');
                if ($ora >= $apertura[0]) {
                  $primaora = $ora;
                  if ($minuti > 10) {
                    $primaora++;
                    $primominuto = 00;
                  } else {
                    $primominuto = 30;
                  }
                } else {
                  $primaora = $apertura[0];
                  $primominuto = 30;
                }
               ?>
              <select id="ora" class="ora" name="ora">
                <?php
                  while($primaora <= $chiusura[0]) {
                    $orario = $primaora . ":" . $primominuto;
                 ?>
                <option value="<?php echo $orario; ?>"><?php echo $orario; ?></option>
                <?php
                  if ($primominuto == 30) {
                    $primominuto = 00;
                    $primaora += 1;
                  } else {
                    $primominuto = 30;
                  }
                }
                 ?>
              </select>
            </div>
          </div>
        </div>
        <div class="cont_btn">
          <button class="btn_spedizione left" type="button" name="undo" onclick="window.location.href='./ordini.php'">Indietro</button>
          <input class="btn_spedizione right" type="submit" name="submitSpedizione" value="Prosegui"/>
        </div>
      </form>

    </section>


  <!-- JQUERY E BOOTSTRAP JS-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="../js/scrollNav.js"></script>
  </body>
</html>
