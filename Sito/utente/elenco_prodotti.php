<?php
$incrementalID = 0;

$usernameFornitore = $_COOKIE["fornitore"];

$servername = "localhost";
$username = "root";
$password = "";
$database = "login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
?>


<!DOCTYPE html>
<html lang="it-IT" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Prodotti</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/header_nav.css">
    <link rel="stylesheet" href="../css/view_prodotti.css">
    <link rel="stylesheet" href="../css/confirmDialog.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link rel="stylesheet" href="../css/chart.css">
  </head>
  <body>
    <header>

      <a href="./elenco_fornitori.php">
        <figure>
          <img id="logo" src="../img/logo.png" alt="logo sito"/>
        </figure>
      </a>
      <figure class="right_figure">
        <img id="carrello" src="../img/carrello.jpg" alt="carrello acquisti"/>
      </figure>
      <div id="navExtended">
          <div class="opac"><span><a href="./elenco_fornitori.php">Home</a></span></div>
          <div><span><a href="./modify_data_ut.php">Modifica account</a></span></div>
          <div class="opac"><span><a href="./ordini.php">Riepilogo ordini</a></span></div>
          <div><span><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></span></div>
      </div>
      <div class="container-toggle">
        <div class="toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </div>
      </div>
    </header>
    <nav id="nav-collapsed">
      <ul>
        <li class="opac"><a href="./elenco_fornitori.php">Home</a></li>
        <li><a href="./modify_data_ut.php">Modifica account</a></li>
        <li  class="opac"><a href="./ordini.php">Riepilogo ordini</a></li>
        <li><a href="../index.php">Logout <span class="fas fa-sign-out-alt"></span></a></li>
      </ul>
    </nav>
    <div class="chart">
      <a href="#">Visualizza elenco ordini</a>
    </div>

    <?php
    $queryDatiRsitorante = "SELECT * FROM register WHERE Username = \"$usernameFornitore\"";
    $result = $conn->query($queryDatiRsitorante);
    while($row = $result->fetch_assoc()) {
      $pathLogo = "../img/" . $row["PathLogo"];
    ?>
    <!--DATI-RISTORANTE-->
    <section id="cont-info-forn" class="toOpac">
      <div class="logo-forn">
        <figure>
          <img src="<?php echo $pathLogo ?>" alt="logo fornitore corrente">
        </figure>
      </div>
      <div class="data-forn">
        <h1><?php echo $row["Negozio"] ?></h1>
        <span><?php echo $row["Indirizzo"] ?></span>
        <span><?php echo $row["Descrizione"] ?></span>
      </div>
      <div class="data-forn hour">
        <span>Orari di apertura Lun - Ven</span>
        <span><?php echo $row["Apertura"] ?> / <?php echo $row["Chiusura"] ?></span>
      </div>
    </section>
    <?php
    }
    ?>


    <section id="container-products" class="toOpac">
      <?php
      $queryCategorie = "SELECT Categoria FROM prodotti WHERE Username = \"$usernameFornitore\"";
      $resultCategorie = $conn->query($queryCategorie);
      $tmpcat = "";
      while($rowCat = $resultCategorie-> fetch_assoc()) {

        /* controllo se la categoria è stata già vagliata ***********************************/
        /* viene effettuato questo controllo in quanto la categoria non è una chiave primaria
        quindi verrebbe creata una categoria per ogni prodotto presente in essa */

        $categoria = $rowCat["Categoria"];
        $ris = strpos($tmpcat, $categoria);
        if(strpos($tmpcat, $categoria) === FALSE) {
          $tmpcat .= $categoria;
        /* fine controllo categoria *********************************************************/
      ?>

      <section class="category">
        <div class="category-title">
          <div class="container-title">
            <h2><?php echo $rowCat["Categoria"] ?></h2>
          </div>
          <div class="container-icon plus">
            <span class="fas fa-plus"></span>
          </div>
          <div class="container-icon minum">
            <span class="fas fa-minus"></span>
          </div>
        </div>
      </section>


      <div class="container-products">
        <!--INFORNAZIONI-SUL-PRODOTTO-->
        <?php
        $categoria = $rowCat["Categoria"];
        $queryProdotti = "SELECT * FROM prodotti WHERE Categoria = \"$categoria\" AND Username = \"$usernameFornitore\"";
        $resultProdotti = $conn->query($queryProdotti);

        while($rowProd = $resultProdotti->fetch_assoc()) {
        ?>
        <section class="row-product">
          <div class="container-information">
            <div class="cont-img-product">
              <figure>
                <?php $pathImage = "../img/prodotti/" . $rowProd["PathImmagine"]; ?>
                <img class="img-product" src="<?php echo $pathImage ?>" alt="immagine prodotto">
              </figure>
            </div>
            <div class="information-product">

                <span class="title-forn"><?php echo $rowProd["Nome"] ?> - &nbsp;<span class="price"><?php echo $rowProd["Prezzo"] ?></span>€</span>

                <span class="title-forn">
                    <?php
                      if($rowProd["Disp"] === "Disponibile") {
                        $disp = "<span class='green'>" . $rowProd["Disp"] . "</span>";
                        echo $disp;
                      } else {
                        $notdisp = "<span class='red'>" . $rowProd["Disp"] . "</span>";
                        echo $notdisp;
                      }
                    ?>
                  </span>
              <span><?php echo $rowProd["Descrizione"] ?></span>
            </div>
          </div>
          <div class="buy-product">
            <label for="quantity<?php echo $incrementalID?>">Quantità</label>
            <input id="quantity<?php echo $incrementalID?>" class="quantity" type="number" name="quantity" value="0" min="0"/>
            <input id="add-car<?php echo $incrementalID; $incrementalID++;?>" class="add-chart add-car" type="submit" name="add-car" value="Aggiungi" onclick="aggiungi('<?php echo $rowProd["Nome"]; ?>', '<?php echo $usernameFornitore ?>', '<?php echo $_COOKIE["user"] ?>', this)"/>
            <span>Total: <span class="total">0.00</span> </span>
          </div>
        </section>
        <?php
        }
        ?>
      </div>

    <?php
      }
    }
    $conn->close();
    ?>
    </section>

    <div id="confirmDialog">
      <div class="cont_dialog contProva">
        <span></span>
        <div class="cont_btn_confirm">
          <button class="button left" type="button" name="buttonDeny" onclick="fadeOutConfirm()">Annulla</button>
        </div>
      </div>
    </div>

    <footer class="toOpac">
      <section class="subtitles-footer">
        <span class="title-forn">Contattaci</span>
        <div id="information">
          <span>Telefono: +39 123456789</span><span>Email: <a href="mailto:campus.gourmet.unibo@gmail.com?Subject=Help%20from%20website">campus.gourmet.unibo@gmail.com</a></span>
        </div>

      </section>
      <section class="subtitles-footer">
        <span class="title-forn">Tipologie di cucina</span>
        <div id="typologies">
          <span>PIADINERIA PIZZERIA HABURGERIA BIRRERIA SUPERMERCATI DISCOUNT CUCINA MEDITERRRANEA</span>
        </div>
      </section>
      <section class="subtitles-footer">
          <span class="title-forn">Su di noi</span>
          <p>Il nostro impegno è offrire un servizio vario e soddisfacente per tutti gli utenti del campus</p>
      </section>

    </footer>


    <!-- JQUERY E BOOTSTRAP JS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="../js/scrollNav.js"></script>
    <script src="../js/prodotti.js"></script>
    <script src="../js/confirmDialog.js"></script>
    <script src="../js/chart.js"></script>
  </body>
</html>
